<?php

namespace MyApp\Exception;

class InvalidPost extends \Exception {
  protected $message = 'Invalid Post!';
}
