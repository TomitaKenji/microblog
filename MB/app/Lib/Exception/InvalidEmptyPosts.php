<?php

namespace MyApp\Exception;

class InvalidEmptyPosts extends \Exception {
  protected $message = 'cannot be empty';
}
