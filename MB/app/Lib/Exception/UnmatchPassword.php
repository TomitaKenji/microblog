<?php

namespace MyApp\Exception;

class UnmatchPassword extends \Exception {
  protected $message = 'Unmatch Password and Confirmed Password!';
}
