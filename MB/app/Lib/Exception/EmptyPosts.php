<?php

namespace MyApp\Exception;

class EmptyPosts extends \Exception {
  protected $message = 'Please enter Username/Email/Password!';
}
