<?php

namespace MyApp\Controller;

class Post extends \MyApp\Controller {

  public function run()
  {
    if (!$this->isLoggedIn()) {
      // login
      header('Location: ' . SITE_URL . '/login.php');
      exit;
    }
  }

   public function post()
   {
    if (isset($_POST['comment'])) {

      if (strlen($_FILES["image"]["name"]) == 0) {
       $this->add();
      } else {
        $this->upload();
       }
     }
  }

  public function comment()
  {
   if (isset($_POST['comment'])) {

     if (strlen($_FILES["image"]["name"]) == 0) {
      $this->addComment();
     } else {
      $this->uploadComment();
      }
    }
 }

  public function share()
  {
    if (isset($_POST['share'])) {
      if (strlen($_FILES["image"]["name"]) == 0) {
     $this->shareAdd();
      } else {
        $this->uploadShare();
       }
    }
    $postModel = new \MyApp\Model\Post();
    $this->setValues('share', $postModel->getShare());
  }


  public function delete()
  {
    if (isset($_POST['delete'])){
       $postModel = new \MyApp\Model\Post();
       $postModel->delete();
       $_SESSION['error'] = 'Delete!';
       header('Location:'.$_SESSION['page']);
       }

  }

  public function uploadShare()
  {
      try {
      // error check
      $this->_validateUpload();
      $this->_validateImageandPost();
      // type check
      $ext = $this->_validateImageType();
      // save
      $savePath = $this->_save($ext);
      $_SESSION['success'] = 'Share!';

      } catch (\Exception $e) {
        $_SESSION['error'] = $e->getMessage();
      return;
      }

      $postModel = new \MyApp\Model\Post();
      $postModel->createShare([
        'user_id'=> $_SESSION['me']->id,
        'comment'=> $_POST['comment'],
        'shared_post_id'=> $_GET['id'],
        'image_name'=> $savePath
      ]);
      header('Location:index.php');
      exit;
  }


  public function upload()
  {
    try {
      // error check
      $this->_validateUpload();
      $this->_validateImageandPost();
      // type check
      $ext = $this->_validateImageType();
      // save
      $savePath = $this->_save($ext);
      $_SESSION['success'] = 'Upload!';

      } catch (\Exception $e) {
      $_SESSION['error'] = $e->getMessage();
      return;
      }

      $postModel = new \MyApp\Model\Post();
      $postModel->createPost([
        'user_id'=> $_SESSION['me']->id,
        'comment'=> $_POST['comment'],
        'image_name'=> $savePath
      ]);
      header('Location:index.php');
      exit;
  }

  public function edit()
  {
     if (isset($_POST['edit'])) {
       if (strlen($_FILES["image"]["name"]) == 0) {
       $this->editAdd();

       } else {
       $this->uploadEdit();
    }
  }
  $postModel = new \MyApp\Model\Post();
  $this->setValues('edit', $postModel->getEdit());
 }

  protected function editAdd()
  {
   try {
   $this->_validatePost();
   } catch (\MyApp\Exception\InvalidEmptyPost $e) {
   $this->setErrors('comment', $e->getMessage());
   } catch (\MyApp\Exception\InvalidPost $e) {
    $this->setErrors('comment', $e->getMessage());
   } catch (\MyApp\Exception\InvalidEmptyPosts $e) {
    $this->setErrors('comment', $e->getMessage());
   }
   $this->setValues('comment', $_POST['comment']);
   if ($this->hasError()) {
   return;
   } else {
   //create post
   $_SESSION['success'] = 'Edit!';
   $postModel = new \MyApp\Model\Post();
   $postModel->createEdit([
  ':comment' => $_POST['comment'],
   ':id' => $_GET['id']
   ]);
   header('Location:'.$_SESSION['page']);
   exit;
 }
}

public function uploadEdit()
{
  try {
    // error check
    $this->_validateUpload();
    $this->_validateImageandPost();
    // type check
    $ext = $this->_validateImageType();
    // save
    $savePath = $this->_save($ext);
    $_SESSION['success'] = 'Edit!';
    } catch (\Exception $e) {
    $this->setErrors('comment', $e->getMessage());
    return;
    }
    $postModel = new \MyApp\Model\Post();
    $postModel->addEdit([
      'id'=> $_GET['id'],
      'comment'=> $_POST['comment'],
      'image_name'=> $savePath
    ]);
    header('Location:'.$_SESSION['page']);
    exit;
}

 protected function shareAdd() {
  try {
  $this->_validatePost();
  } catch (\MyApp\Exception\InvalidEmptyPost $e) {
    $this->setErrors('comment', $e->getMessage());
  } catch (\MyApp\Exception\InvalidPost $e) {
    $this->setErrors('comment', $e->getMessage());
  } catch (\MyApp\Exception\InvalidEmptyPosts $e) {
    $this->setErrors('comment', $e->getMessage());
  }
    $this->setValues('comment', $_POST['comment']);
    if ($this->hasError()) {
    return;
  } else {
  //create post
  $_SESSION['success'] = 'Share!';
  $postModel = new \MyApp\Model\Post();
  $postModel->createShare([
    'user_id'=> $_SESSION['me']->id,
    'comment'=> $_POST['comment'],
    ':shared_post_id' => $_GET['id']
  ]);
  header('Location: index.php');
  exit;
  }
}
  protected function add()
  {
   try {
   $this->_validatePost();
     } catch (\MyApp\Exception\InvalidEmptyPost $e) {
     $this->setErrors('comment', $e->getMessage());
     } catch (\MyApp\Exception\InvalidPost $e) {
     $this->setErrors('comment', $e->getMessage());
     } catch (\MyApp\Exception\InvalidEmptyPosts $e) {
     $this->setErrors('comment', $e->getMessage());
     }
     $this->setValues('comment', $_POST['comment']);
     if ($this->hasError()) {
     return;
     }
     $_SESSION['success'] = 'Comment!';
     $postModel = new \MyApp\Model\Post();
     $postModel->createComment([
       'user_id'=> $_SESSION['me']->id,
       'comment'=> $_POST['comment']
     ]);
     header('Location:index.php');
     exit;
    }

    protected function addComment()
    {
     try {
     $this->_validatePost();
       } catch (\MyApp\Exception\InvalidEmptyPost $e) {
       $this->setErrors('comment', $e->getMessage());
       } catch (\MyApp\Exception\InvalidPost $e) {
       $this->setErrors('comment', $e->getMessage());
       } catch (\MyApp\Exception\InvalidEmptyPosts $e) {
       $this->setErrors('comment', $e->getMessage());
       }
       $this->setValues('comment', $_POST['comment']);
       if ($this->hasError()) {
       return;
       }
       $_SESSION['success'] = 'Comment!';
       $postModel = new \MyApp\Model\Post();
       $postModel->createComment([
         'user_id'=> $_SESSION['me']->id,
         'comment'=> $_POST['comment'],
         'commented_post_id'=> $_GET['id']
         // 'image_name'=> $savePath
       ]);
       header('Location:comment.php?id='.$_GET['id']);
       exit;
   }

   public function uploadComment()
   {
     try {
       // error check
       $this->_validateUpload();
       $this->_validateImageandPost();
       // type check
       $ext = $this->_validateImageType();
       // save
       $savePath = $this->_save($ext);
       $_SESSION['success'] = 'Comment!';
       } catch (\Exception $e) {
       $this->setErrors('comment', $e->getMessage());
       return;
       }
       $postModel = new \MyApp\Model\Post();
       $postModel->createUpload([
         'user_id'=> $_SESSION['me']->id,
         'comment'=> $_POST['comment'],
         'commented_post_id'=> $_GET['id'],
         'image_name'=> $savePath
       ]);
       header('Location:comment.php?id='.$_GET['id']);
       exit;
   }
   public function getResults()
   {
     $success = null;
     $error = null;
     if (isset($_SESSION['success'])) {
       $success = $_SESSION['success'];
       unset($_SESSION['success']);
     }
     if (isset($_SESSION['error'])) {
       $error = $_SESSION['error'];
       unset($_SESSION['error']);
     }
     return [$success, $error];
   }

     private function _validatePost()
     {
      if (!mb_ereg("^.{1,140}$", $_POST['comment'])){
        throw new \MyApp\Exception\InvalidEmptyPost();
      }

      if (mb_ereg_match("^(\s|　)+$",$_POST['comment'])) {
        throw new \MyApp\Exception\InvalidEmptyPosts();
      }
    }

    private function _validateImageType()
    {
      $this->_imageType = exif_imagetype($_FILES['image']['tmp_name']);
      switch($this->_imageType) {
        case IMAGETYPE_GIF:
          return 'gif';
        case IMAGETYPE_JPEG:
          return 'jpg';
        case IMAGETYPE_PNG:
          return 'png';
        default:
         throw new  \Exception('PNG/JPEG/GIF only!');
      }
    }
    private function _validateUpload()
    {
      if (!isset($_FILES['image']) || !isset($_FILES['image']['error'])) {
        throw new \Exception('Upload Error!');
      }

      switch($_FILES['image']['error']) {
        case UPLOAD_ERR_OK:
          return true;
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
          throw new \Exception('Over 1MB!');
      }
    }

    private function _validateImageandPost()
    {
      if (!mb_ereg("^.{1,140}$", $_POST['comment'])){
        throw new \MyApp\Exception\InvalidEmptyPost();
      }

      if (mb_ereg_match("^(\s|　)+$",$_POST['comment'])) {
        throw new \MyApp\Exception\InvalidEmptyPosts();
      }
   }

    private function _save($ext)
    {
      $this->_imageFileName = sprintf(
        '%s_%s.%s',
        time(),
        sha1(uniqid(mt_rand(), true)),
        $ext
      );
      $savePath = IMAGES_DIR . '/' . $this->_imageFileName;
      $res = move_uploaded_file($_FILES['image']['tmp_name'], $savePath);
      if ($res === false) {
        throw new \Exception('Could not upload!');
      }
      return $savePath;
    }
}
