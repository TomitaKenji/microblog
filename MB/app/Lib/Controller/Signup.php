<?php

namespace MyApp\Controller;

class Signup extends \MyApp\Controller
{

  public function run()
  {
    if ($this->isLoggedIn()) {
      header('Location: ' . SITE_URL .'/index.php');
      exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $this->postProcess();
    }
  }

  protected function postProcess()
  {
    // validate
     $username = $_POST['username'];
     $email = $_POST['email'];
     $userModel = new \MyApp\Model\User();

    if (!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']) {
       echo "Invalid Token!";
       exit;
       }
    if ($userModel->userExists($username)){
       $error['username'] = 'already exist Username!';
       $this->setErrors('username', $error['username']);
      }
    if (!preg_match('/^[0-9a-zA-Z]*$/', $username)){
       $error['username'] = 'Only number and letter';
       $this->setErrors('username', $error['username']);
      }
    if (!preg_match("/^.{5,15}$/",$username)) {
      $error['username'] = 'between 5 to 15 characters';
      $this->setErrors('username', $error['username']);
      }
    if ($username === ''){
       $error['username'] = 'input your Username!';
       $this->setErrors('username', $error['username']);
      }
    if ($userModel->emailExists($email)) {
       $error['email'] = 'already exist E-mail!';
       $this->setErrors('email', $error['email']);
       }
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
       $error['email'] = 'Invalid E-mail!';
       $this->setErrors('email', $error['email']);
       }
    if ($_POST['email'] === ''){
       $error['email'] = 'input your email!';
       $this->setErrors('email', $error['email']);
     }
    if (!preg_match('/\A(?=.*?[a-z])(?=.*?\d)[a-z\d]{8,100}+\z/i', $_POST['password'])) {
       $error['password'] = 'Should have number and character!';
       $this->setErrors('password', $error['password']);
       }
    if (!preg_match("/^.{8,100}$/", $_POST['password'])) {
       $error['password'] = 'more than 8 letters';
       $this->setErrors('password', $error['password']);
       }
    if ($_POST['password'] === ''){
       $error['password'] = 'input your password!';
       $this->setErrors('password', $error['password']);
      }
    if ($_POST['password'] !== $_POST['confirmed_password']){
       $error['confirmed_password'] = 'Unmatch Password!';
       $this->setErrors('confirmed_password', $error['confirmed_password']);
       }


      $this->setValues('username', $_POST['username']);
      $this->setValues('email', $_POST['email']);

     if ($this->hasError()) {
      return;

     } else {
      // create user
      $userModel = new \MyApp\Model\User();
      $userModel->create([
        'username'=> $_POST['username'],
        'email' => $_POST['email'],
        'password' => $_POST['password']
      ]);

      // redirect to login
      $_SESSION['success'] = 'Registration Successful,Please enter';
      $userModel->defaultImage($username);
      header('Location:login.php');
      exit;
      }

     }

   public function getResults()
   {
     $success = null;
     $error = null;
     if (isset($_SESSION['success'])) {
       $success = $_SESSION['success'];
       unset($_SESSION['success']);
     }
     if (isset($_SESSION['error'])) {
       $error = $_SESSION['error'];
       unset($_SESSION['error']);
     }
     return [$success, $error];
   }
}
