<?php

namespace MyApp\Controller;

class Index extends \MyApp\Controller
{

  public function run()
  {
    if (!$this->isLoggedIn()) {
      // login
      header('Location: ' . SITE_URL . '/login.php');
      exit;
    }
    // get users info
    $userModel = new \MyApp\Model\User();
    $postModel = new \MyApp\Model\Post();
    $this->setValues('users', $userModel->findAll());
    // get me info
    $this->setValues('me', $userModel->findme());
    // get follows info
    $this->setValues('follows', $userModel->follow());
    //get follower
    $this->setValues('followers', $userModel->follower());
    //get user
    $this->setValues('comments', $postModel->getPage());
    
  }

}
