<?php

namespace MyApp\Controller;

class Update extends \MyApp\Controller
{

  public function run()
  {

    if (!$this->isLoggedIn()) {
      // login
      header('Location: ' . SITE_URL . '/login.php');
      exit;
    }
    if (isset($_POST['toUpdate'])) {

      $this->updateProcess();
    }
    if (isset($_POST['update'])) {

      $this->saveProcess();
    }
  }

  protected function saveProcess()
  {
    $userModel = new \MyApp\Model\User();
    $username = $_POST['username'];
    $email = $_POST['email'];
    if ($_SESSION['me']->username !== $username){
    if ($userModel->userExists($username)){
       $error['username'] = 'already exist Username!';
       $this->setErrors('username', $error['username']);
      }
    }
    if (!preg_match('/^[0-9a-zA-Z]*$/', $username)){
       $error['username'] = 'Only number and letter';
       $this->setErrors('username', $error['username']);
      }
    if (!preg_match("/^.{5,15}$/",$username)) {
      $error['username'] = 'between 5 to 15 characters';
      $this->setErrors('username', $error['username']);
      }
    if ($username === ''){
       $error['username'] = 'input your Username!';
       $this->setErrors('username', $error['username']);
      }
    if ($_SESSION['me']->email !== $email){
    if ($userModel->emailExists($email)) {
       $error['email'] = 'already exist E-mail!';
       $this->setErrors('email', $error['email']);
       }
     }
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
       $error['email'] = 'Invalid E-mail!';
       $this->setErrors('email', $error['email']);
       }
    if ($_POST['email'] === ''){
       $error['email'] = 'input your email!';
       $this->setErrors('email', $error['email']);
      }
      if ($this->hasError()) {
       return;

      } else {
       // update user info
       $userModel = new \MyApp\Model\User();
       $userModel->update([
         'username'=> $_POST['username'],
         'email' => $_POST['email']
       ]);
       unset($_SESSION['update']);
       $_SESSION['success'] = 'Updated your information!';
      }
    }
  protected function updateProcess()
  {
    // validate

    if ($_POST['password'] !== $_POST['confirmed_password']){
      $error['password'] = 'Unmatch Password!';
      $this->setErrors('password', $error['password']);
      }
    if ($_POST['password'] === ''){
       $error['password'] = 'input your password!';
       $this->setErrors('password', $error['password']);
      }

     try {
      $userModel = new \MyApp\Model\User();
      $user = $userModel->toUpdate([
       'email' => $_SESSION['me']->email,
       'password' => $_POST['password']
      ]);

    } catch (\Exception $e) {
      $this->setErrors('password', $e->getMessage());
      //  echo $e->getMessage();
      // exit;
     return;
    }
    if ($this->hasError()) {
     return;
   } else {

     $_SESSION['update'] = 'Welcome';
      header('Location:update.php');
      exit;
    }
 }



  private function _validate()
  {
    if (!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']) {
      echo "Invalid Token!";
      exit;
    }
    if (!preg_match('/\A[a-zA-Z0-9]+\z/', $_POST['username'])) {
      throw new \MyApp\Exception\InvalidUsername();
    }
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
      throw new \MyApp\Exception\InvalidEmail();
    }
    if (!preg_match('/\A[a-zA-Z0-9]+\z/', $_POST['password'])) {
      throw new \MyApp\Exception\InvalidPassword();
    }
    if ($_POST['password'] !== $_POST['confirmed_password']){
      throw new \MyApp\Exception\UnmatchPassword();
    }
  }
}
