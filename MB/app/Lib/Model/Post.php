<?php

namespace MyApp\Model;

class Post extends \MyApp\Model {

  private $_db;

  public function __construct() {
    try {
      $this->_db = new \PDO(DSN, DB_USERNAME, DB_PASSWORD);
      $this->_db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    } catch (\PDOException $e) {
      echo $e->getMessage();
      exit;
    }
  }

  public function delete()
  {

    $stmt = $this->_db->prepare("update posts set deleted = 1 where id = :id limit 1");
    $stmt->execute([
      ":id" => $_POST['delete']
    ]);
  }

  public function createPicture($values)
  {
    $stmt = $this->_db->prepare("update profile_pictures set images_name = :images_name where user_id = :user_id");
    $stmt->execute([
      ":user_id" => $_SESSION['me']->id,
      ':images_name' =>$values['images_name']
    ]);
  }

  public function getPage()
  {
    $page = isset($_GET['page']) ? h($_GET['page']) : 1;
    $offset = COMMENTS_PER_PAGE * ($page - 1);
    $stmt = $this->_db->prepare("select DISTINCT p.id, p.comment, p.shared_post_id, p.commented_post_id, p.created, p.deleted, u.username, pp.images_name, f.followed_id FROM posts AS p INNER JOIN users AS u ON p.user_id = u.id INNER JOIN profile_pictures AS pp ON p.user_id = pp.user_id INNER JOIN followers AS f ON p.user_id =f.followed_id where f.user_id = :id and p.commented_post_id is null and p.deleted is null ORDER BY `p`.`id` DESC");
    $stmt->execute([
      ':id' => $_SESSION['me']->id
    ]);
    $comments = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    // var_dump($comments);
    // exit();

    $total = count($comments);
    $totalPages = ceil($total / COMMENTS_PER_PAGE);
    $sql = "select DISTINCT p.id, p.comment, p.shared_post_id, p.image_name, p.created, p.updated, u.username, pp.images_name, f.followed_id FROM posts AS p INNER JOIN users AS u ON p.user_id = u.id INNER JOIN profile_pictures AS pp ON p.user_id = pp.user_id INNER JOIN followers AS f ON p.user_id =f.followed_id where f.user_id = :id and  p.commented_post_id is null and p.deleted is null ORDER BY `p`.`id` DESC limit ".$offset.", ".COMMENTS_PER_PAGE;
    $stmt = $this->_db->prepare($sql);
    $stmt->execute([
      ':id' => $_SESSION['me']->id
    ]);
    return $stmt->fetchAll(\PDO::FETCH_ASSOC);
  }

  public function getUsercomment($id)
  {
    $sql = "select DISTINCT p.id, p.comment, p.shared_post_id, p.image_name, p.created, p.updated, p.deleted, u.username FROM posts AS p INNER JOIN users AS u ON p.user_id = u.id INNER JOIN profile_pictures AS pp ON p.user_id = pp.user_id INNER JOIN followers AS f ON p.user_id =f.followed_id where u.id = :id and p.deleted is null and p.commented_post_id is null ORDER BY `p`.`id` DESC";
    $stmt = $this->_db->prepare($sql);
    $stmt->execute([
      ':id' => $id
    ]);
    return  $stmt->fetchAll(\PDO::FETCH_ASSOC);
  }
  public function getEdit()
  {
    $stmt = $this->_db->prepare("select user_id, comment, image_name from posts where id = :postid limit 1");
    $stmt->execute([
      ':postid' => $_GET['id']
    ]);
    return $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  }

  public function createEdit()
  {
    $stmt = $this->_db->prepare("update posts set comment = :comment, updated = now() where id =:id ");
    $stmt->execute([
      ':comment' => $_POST['comment'],
      ':id' => $_GET['id']
    ]);
  }

  public function addEdit($values)
  {
    $stmt = $this->_db->prepare("update posts set comment = :comment, image_name = :image_name, updated = now() where id = :id limit 1");
    $stmt->execute([
      ':id' => $_GET['id'],
      ':comment' => $_POST['comment'],
      ':image_name' =>$values['image_name']
    ]);
  }

  public function getShare()
  {
    $stmt = $this->_db->prepare("select DISTINCT u.id, username, comment, image_name, pp.images_name
    FROM users AS u LEFT OUTER JOIN posts AS p on u.id = p.user_id
    LEFT OUTER JOIN profile_pictures AS pp on u.id = pp.user_id where p.id = :id order by pp.id DESC limit 1");
    $stmt->execute([
      ":id" => $_GET['id']
    ]);
    return $stmt->fetch(\PDO::FETCH_ASSOC);
  }

  public function createShare($values)
  {
    $stmt = $this->_db->prepare("insert into posts (user_id, comment, shared_post_id, image_name, created)
    values(:user_id, :comment, :shared_post_id, :image_name, now())");
    $res = $stmt->execute([
      ':user_id' => $_SESSION['me']->id,
      ':comment' => $_POST['comment'],
      ':shared_post_id' => $_GET['id'],
      ':image_name'=> $values['image_name']
    ]);
  }

  public function createPost($values)
  {
      $stmt = $this->_db->prepare("insert into posts ( user_id, comment, image_name, created)
      values ( :user_id, :comment, :image_name, now())");
      $res = $stmt->execute([
        ':user_id' => $_SESSION['me']->id,
        ':comment' => $values['comment'],
        ':image_name' =>$values['image_name']
      ]);
  }

  public function createComment($values)
  {
      $stmt = $this->_db->prepare("insert into posts ( user_id, comment, commented_post_id, created)
      values ( :user_id, :comment, :commented_post_id, now())");
      $res = $stmt->execute([
        ':user_id' => $_SESSION['me']->id,
        ':comment' => $values['comment'],
        ':commented_post_id' =>$_GET['id']
      ]);
  }
  public function createUpload($values)
  {
    // echo $values['comment'];
    // exit();
      $stmt = $this->_db->prepare("insert into posts ( user_id, comment, image_name, commented_post_id, created)
      values ( :user_id, :comment, :image_name, :commented_post_id, now())");
      $res = $stmt->execute([
        ':user_id' => $_SESSION['me']->id,
        ':comment' => $values['comment'],
        ':commented_post_id' =>$_GET['id'],
        ':image_name' =>$values['image_name']
      ]);
  }
  public function getAll()
  {
    $stmt = $this->_db->query("select * from posts order by id desc");
    return $stmt->fetchAll(\PDO::FETCH_OBJ);
  }

  public function sharedComment($shared_id)
  {
    $sql = "select DISTINCT u.id, u.username, p.comment, p.image_name, p.created, p.updated, p.deleted, pp.images_name FROM users AS u LEFT OUTER JOIN posts AS p on u.id = p.user_id LEFT OUTER JOIN profile_pictures AS pp on u.id = pp.user_id where p.id = :shared_post_id order by pp.id DESC limit 1";
    $stmt = $this->_db->prepare($sql);
    $stmt->execute([
      ":shared_post_id" => $shared_id
    ]);
    $share_result = $stmt->fetch(\PDO::FETCH_ASSOC);
  }

  public function existPost()
  {
    $stmt = $this->_db->prepare("select * from posts where id = :id and deleted is null");
    $stmt->execute([
      ':id' => $_GET['id']
    ]);
    $res = $stmt->fetch(\PDO::FETCH_ASSOC);

    if ($res === false) {
      $_SESSION['error'] = 'deleted your post or dont exist post';
      header('Location: index.php');
      exit;
    }
  }

  public function cheackPost()
  {
    $stmt = $this->_db->prepare("select count(*) from posts where user_id = :user_id and id = :id");
    $stmt->execute([
      ":user_id" => $_SESSION['me']->id,
      ':id' => $_GET['id']
    ]);
    $res = $stmt->fetch(\PDO::FETCH_ASSOC);
    if ($res['count(*)'] == 0) {
      $_SESSION['error'] = 'Sorry, cannot move';
      header('Location: index.php');
      exit;
    }
  }

  public function cheackComment()
  {
    $stmt = $this->_db->prepare("select * FROM followers LEFT JOIN posts ON followers.followed_id = posts.user_id where posts.id = :id and followers.user_id = :user_id");
    $stmt->execute([
      ':id' => $_GET['id'],
      ':user_id'=> $_SESSION['me']->id
    ]);
    $res = $stmt->fetch(\PDO::FETCH_ASSOC);
    if ($res == false) {
      $_SESSION['error'] = 'Sorry, cannot move';
      header('Location: index.php');
      exit;
    }
  }

  public function getPost()
  {
    $stmt = $this->_db->prepare("select DISTINCT u.username, p.id, p.user_id, p.comment, p.shared_post_id, p.image_name, p.created, p.updated, p.deleted, pp.images_name
    FROM users AS u LEFT OUTER JOIN posts AS p on u.id = p.user_id
    LEFT OUTER JOIN profile_pictures AS pp on u.id = pp.user_id where p.id = :id order by pp.id DESC");
    $stmt->execute([
      ":id" => $_GET['id']
    ]);
    return $stmt->fetch(\PDO::FETCH_ASSOC);
  }

  public function findUser($c_p_i)
  {
    $stmt = $this->_db->prepare("select u.username from users AS u LEFT OUTER JOIN posts AS p on u.id = p.user_id where commented_post_id = :id limit 1");
    $stmt->execute([
      ":id" => $c_p_i
    ]);
    return $stmt->fetch(\PDO::FETCH_ASSOC);
  }
}
