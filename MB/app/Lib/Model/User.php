<?php

namespace MyApp\Model;

class User extends \MyApp\Model {

  public function create($values)
  {
    $stmt = $this->db->prepare("insert into users ( username, email, password, created)
    values (:username, :email, :password, now())");
    $res = $stmt->execute([
      ':username' => $values['username'],
      ':email' => $values['email'],
      ':password' => password_hash($values['password'], PASSWORD_DEFAULT)
    ]);
    if ($res === false) {
      throw new \MyApp\Exception\DuplicateEmail();
    }
  }

  public function userExists($username)
  {
    $stmt = $this->db->prepare("select * FROM users where username = :username limit 1");
    $user = $stmt->execute([
      ':username' => $username
    ]);
    $user = $stmt->fetch(\PDO::FETCH_ASSOC);
    return $user ? true : false;
  }

  public function userExist()
  {
    $stmt = $this->db->prepare("select * FROM users where id = :id limit 1");
    $user = $stmt->execute([
      ':id' => $_GET['id']
    ]);
    $user = $stmt->fetch(\PDO::FETCH_ASSOC);
    if ($user === false) {
      $_SESSION['error'] = 'Sorry, dont exist';
      header('Location: index.php');
      exit;
    }
  }
  public function emailExists($email)
  {
    $stmt = $this->db->prepare("select * FROM users where email = :email limit 1");
    $user = $stmt->execute([
      ':email' => $email
    ]);
    $user = $stmt->fetch(\PDO::FETCH_ASSOC);
    return $user ? true : false;
  }

  public function defaultImage($username)
  {
    $stmt = $this->db->prepare("insert into profile_pictures(user_id, created)
    select id , created FROM users where username = :username ");
    $stmt->execute([
      ':username' => $username
    ]);
  }
  public function defaultFollow()
  {
    $stmt = $this->db->prepare("insert into followers (user_id, followed_id, created)
            values (:user_id, :followed_id, now())");
    $stmt->execute([
      ':user_id' => $_SESSION['me']->id,
      ':followed_id' => $_SESSION['me']->id
    ]);
  }
  public function toUpdate($values)
  {
    $stmt = $this->db->prepare("select * from users where email = :email");
    $stmt->execute([
      ':email' => $_SESSION['me']->email
    ]);
    $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
    $user = $stmt->fetch();

    if (empty($user)) {
      throw new \Exception('Unmatch Password');
    }
    if (!password_verify($values['password'], $user->password)) {
      throw new \Exception('Unmatch Password');
    }
    return $user;
    }
  public function update($values)
  {
    $stmt = $this->db->prepare("update users set username = :username, email=:email WHERE id = :id");
    $stmt->execute([
      ':username' => $values['username'],
      ':email' => $values['email'],
      ':id' => $_SESSION['me']->id
    ]);
  }

  public function login($values)
  {
    $stmt = $this->db->prepare("select * from users where email = :email");
    $stmt->execute([
      ':email' => $values['email']
    ]);
    $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
    $user = $stmt->fetch();

    if (empty($user)) {
      throw new \MyApp\Exception\UnmatchEmailOrPassword();
    }
    if (!password_verify($values['password'], $user->password)) {
      throw new \MyApp\Exception\UnmatchEmailOrPassword();
    }
    return $user;
    }

    public function follows()
    {
      $stmt = $this->db->prepare("select * FROM users LEFT JOIN followers
      ON users.id = followers.followed_id
      where user_id = :id and not users.id = :id order by users.id");
      $stmt->execute([
        ':id' => $_GET['id']
      ]);
      return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function followers()
    {
      $stmt = $this->db->prepare("select distinct users.id, users.username
      FROM users LEFT JOIN followers
      ON users.id = followers.user_id
      where followed_id = :followed_id and not users.id = :followed_id order by users.id");
      $stmt->execute([
        ':followed_id' => $_GET['id']
      ]);
      return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function follower()
    {
      $stmt = $this->db->prepare("select distinct users.id, users.username
      FROM users LEFT JOIN followers
      ON users.id = followers.user_id
      where followed_id = :followed_id and not users.id = :followed_id order by users.id");
      $stmt->execute([
        ':followed_id' => $_SESSION['me']->id
      ]);
      return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function follow()
    {
      $stmt = $this->db->prepare("select * FROM users LEFT JOIN followers
      ON users.id = followers.followed_id
      where user_id = :id and not users.id = :id order by users.id");
      $stmt->execute([
        ':id' => $_SESSION['me']->id
      ]);
      return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function followerUser($id)
    {
      $stmt = $this->db->prepare("select distinct *
      FROM users LEFT JOIN followers
      ON users.id = followers.user_id
      where followed_id = :followed_id and not users.id = :followed_id order by users.id");
      $stmt->execute([
        ':followed_id' => $id
      ]);
      return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function followUser($id)
    {
      $stmt = $this->db->prepare("select * FROM users LEFT JOIN followers
      ON users.id = followers.followed_id
      WHERE user_id = :id and not users.id = :id order by users.id");
      $stmt->execute([
        ':id' => $id
      ]);
      return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    public function findUser($id)
    {
      $stmt = $this->db->prepare("select * FROM users
      INNER JOIN profile_pictures AS pp ON users.id = pp.user_id
      WHERE users.id = :id order by pp.id desc limit 1");
      $stmt->execute([
        ':id' => $id
      ]);
      return $stmt->fetch(\PDO::FETCH_ASSOC);
    }
   public function findme()
   {
     $stmt = $this->db->prepare("select * FROM users
     INNER JOIN profile_pictures AS pp ON users.id = pp.user_id
     WHERE users.id = :id ORDER BY pp.id desc limit 1");
     $stmt->execute([
       ':id' => $_SESSION['me']->id
     ]);
     return $stmt->fetch(\PDO::FETCH_ASSOC);
   }

   public function findAll()
   {
     $stmt = $this->db->query("select * from users order by id");
     $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
     return $stmt->fetchAll();
   }
}
