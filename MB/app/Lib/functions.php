<?php

function h($s) {
  return nl2br(htmlspecialchars($s, ENT_QUOTES, 'UTF-8'));
}

function ht($s) {
  return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
}

function connectDb() {
    try {
        return new PDO(DSN, DB_USERNAME, DB_PASSWORD,[PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,]);
    } catch (PDOException $e) {
        echo $e->getMessage();
        exit;
    }
}

function  login() {
  if (empty($_SESSION['me'])) {
    header('Location:login.php');
    exit;
  }
}

function emailExists($email, $dbh)
 {
    $sql = "select * from users where email = :email limit 1";
    $stmt = $dbh->prepare($sql);
    $stmt->execute(array(":email" => $email));
    $user = $stmt->fetch();
    return $user ? true : false;
}

function userExists($username, $dbh)
 {
    $sql = "select * from users where username = :username limit 1";
    $stmt = $dbh->prepare($sql);
    $stmt->execute(array(":username" => $username));
    $user = $stmt->fetch();
    return $user ? true : false;
}

function getUser($email, $password, $dbh)
{
    $sql = "select * from users where email = :email and password = :password limit 1";
    $stmt = $dbh->prepare($sql);
    $stmt->execute(array(":email"=>$email, ":password"=>$password));
    $user = $stmt->fetch();
    return $user ? $user : false;
}

function getUserImage($followed_id, $dbh)
{
$sql = "select images_name from profile_pictures where user_id = :followed_id order by id desc limit 1";
$stmt = $dbh->prepare($sql);
$stmt->execute([
  ":followed_id" => $followed_id
]);
 return $res = $stmt->fetch(PDO::FETCH_ASSOC);
}
