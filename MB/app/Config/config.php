<?php
require_once(__DIR__ . '/autoload.php');
require_once(__DIR__ . '/../Lib/functions.php');
session_cache_limiter('none');
session_start();
ini_set('display_errors', 1);
// DB
define('DSN', 'mysql:dbname=MB;host=localhost;charset=utf8mb4');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('SITE_URL','/php/MB/app/public_html');

// upload images
define('MAX_FILE_SIZE', 1 * 1024 * 1024); // 1MB
define('THUMBNAIL_WIDTH', 400);
define('IMAGES_DIR','images');
define('THUMBNAIL_DIR','thumbs');

define('COMMENTS_PER_PAGE', 15);
