c-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- ホスト: localhost
-- 生成日時: 2019 年 11 月 11 日 12:54
-- サーバのバージョン： 5.6.45
-- PHP のバージョン: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `MB`
--
CREATE DATABASE IF NOT EXISTS `MB` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `MB`;

-- --------------------------------------------------------

--
-- テーブルの構造 `followers`
--

CREATE TABLE `followers` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `followed_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `followed_id`, `created`) VALUES
(78, 36, 35, '2019-10-29 07:16:16'),
(79, 37, 35, '2019-10-29 07:38:16'),
(80, 37, 37, '2019-10-29 07:39:59'),
(82, 35, 36, '2019-10-29 10:37:58'),
(85, 40, 40, '2019-10-30 06:11:06'),
(86, 48, 47, '2019-10-30 07:46:11'),
(87, 48, 41, '2019-10-30 07:48:16'),
(88, 40, 38, '2019-10-30 10:35:06'),
(93, 38, 41, '2019-10-31 05:12:47'),
(95, 38, 35, '2019-10-31 05:22:10'),
(98, 38, 40, '2019-10-31 06:00:24'),
(99, 48, 38, '2019-10-31 07:17:34'),
(100, 49, 49, '2019-10-31 07:51:31'),
(103, 47, 47, '2019-10-31 09:40:29'),
(117, 53, 38, '2019-11-02 07:48:03'),
(118, 53, 53, '2019-11-02 07:50:45'),
(119, 53, 40, '2019-11-02 07:53:43'),
(121, 54, 54, '2019-11-02 09:37:03'),
(123, 54, 38, '2019-11-02 09:37:46'),
(124, 54, 121, '2019-11-02 09:55:58'),
(127, 55, 54, '2019-11-02 10:01:10'),
(129, 55, 55, '2019-11-02 11:07:25'),
(130, 53, 53, '2019-11-02 13:40:57'),
(132, 38, 38, '2019-11-02 13:57:55'),
(155, 58, 150, '2019-11-03 05:04:38'),
(157, 47, 47, '2019-11-03 05:56:59'),
(158, 47, 58, '2019-11-03 05:57:24'),
(198, 58, 58, '2019-11-03 09:57:29'),
(268, 60, 60, '2019-11-03 23:22:40'),
(280, 61, 61, '2019-11-04 01:58:20'),
(281, 61, 35, '2019-11-04 01:58:36'),
(282, 58, 58, '2019-11-04 03:55:01'),
(299, 58, 60, '2019-11-04 06:21:13'),
(302, 58, 58, '2019-11-04 07:51:51'),
(303, 58, 58, '2019-11-04 09:05:05'),
(304, 203, 203, '2019-11-05 03:12:30'),
(305, 58, 58, '2019-11-05 09:17:07'),
(307, 205, 205, '2019-11-06 05:04:09'),
(309, 205, 203, '2019-11-07 02:30:49'),
(310, 58, 58, '2019-11-07 05:12:01'),
(316, 58, 205, '2019-11-07 05:42:23'),
(327, 206, 206, '2019-11-07 08:29:23'),
(338, 210, 210, '2019-11-09 05:16:09'),
(413, 207, 207, '2019-11-10 13:21:56'),
(428, 207, 210, '2019-11-11 01:40:47'),
(429, 207, 206, '2019-11-11 01:40:48'),
(430, 207, 209, '2019-11-11 01:40:50'),
(431, 207, 58, '2019-11-11 01:40:52'),
(432, 212, 212, '2019-11-11 02:22:37'),
(467, 212, 207, '2019-11-11 03:43:27'),
(470, 212, 206, '2019-11-11 03:44:58'),
(471, 212, 210, '2019-11-11 03:55:03'),
(472, 212, 209, '2019-11-11 03:55:10'),
(473, 206, 206, '2019-11-11 04:54:13'),
(474, 206, 206, '2019-11-11 04:59:45'),
(482, 205, 205, '2019-11-11 07:30:18'),
(484, 205, 58, '2019-11-11 07:32:41'),
(485, 58, 58, '2019-11-11 07:49:59'),
(486, 58, 58, '2019-11-11 07:59:16'),
(487, 206, 206, '2019-11-11 08:13:24'),
(488, 206, 206, '2019-11-11 08:37:00'),
(489, 206, 206, '2019-11-11 09:17:42'),
(491, 206, 206, '2019-11-11 10:04:31'),
(492, 206, 206, '2019-11-11 11:23:52'),
(494, 206, 207, '2019-11-11 12:51:05'),
(495, 206, 212, '2019-11-11 12:51:08');

-- --------------------------------------------------------

--
-- テーブルの構造 `likes`
--

CREATE TABLE `likes` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `posts_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `posts_id`, `created`) VALUES
(24, 35, 191, '2019-10-29 02:25:43'),
(25, 36, 192, '2019-10-29 02:26:15'),
(26, 37, 192, '2019-10-29 07:38:06'),
(27, 37, 194, '2019-10-29 07:39:21'),
(28, 37, 193, '2019-10-29 08:25:17'),
(29, 37, 196, '2019-10-29 09:02:56'),
(32, 35, 196, '2019-10-29 10:37:33'),
(33, 38, 204, '2019-10-29 13:59:34'),
(34, 38, 203, '2019-10-29 13:59:36'),
(35, 40, 204, '2019-10-30 01:13:40'),
(36, 40, 202, '2019-10-30 01:13:43'),
(38, 40, 207, '2019-10-30 08:38:33'),
(41, 40, 208, '2019-10-30 09:08:19'),
(42, 40, 206, '2019-10-30 09:08:22'),
(43, 40, 209, '2019-10-30 09:13:17'),
(44, 40, 228, '2019-10-30 12:53:04'),
(46, 38, 237, '2019-10-31 04:55:24'),
(47, 48, 219, '2019-10-31 07:18:00'),
(48, 48, 220, '2019-10-31 07:18:03'),
(49, 48, 221, '2019-10-31 07:18:05'),
(50, 49, 253, '2019-10-31 07:50:50'),
(51, 49, 251, '2019-10-31 07:50:53'),
(53, 47, 278, '2019-10-31 09:39:40'),
(54, 47, 271, '2019-10-31 09:39:41'),
(55, 51, 271, '2019-10-31 13:40:45'),
(59, 51, 279, '2019-10-31 13:43:28'),
(61, 38, 249, '2019-11-01 02:41:07'),
(62, 38, 238, '2019-11-01 02:41:13'),
(63, 38, 365, '2019-11-02 02:40:18'),
(67, 55, 408, '2019-11-02 11:04:20'),
(68, 55, 399, '2019-11-02 11:26:38'),
(71, 55, 404, '2019-11-02 13:23:46'),
(72, 55, 410, '2019-11-02 13:27:33'),
(76, 55, 412, '2019-11-02 13:35:35'),
(77, 55, 413, '2019-11-02 13:36:24'),
(82, 58, 418, '2019-11-03 04:39:21'),
(83, 58, 419, '2019-11-03 05:04:03'),
(85, 58, 427, '2019-11-03 08:59:49'),
(86, 60, 430, '2019-11-03 10:28:57'),
(94, 60, 400, '2019-11-03 14:30:20'),
(104, 60, 428, '2019-11-03 15:01:55'),
(106, 60, 426, '2019-11-03 15:09:26'),
(107, 60, 431, '2019-11-03 23:30:55'),
(108, 60, 433, '2019-11-04 01:52:11'),
(109, 61, 437, '2019-11-04 03:41:33'),
(110, 61, 436, '2019-11-04 03:42:31'),
(111, 58, 416, '2019-11-04 03:55:16'),
(118, 58, 438, '2019-11-04 04:46:45'),
(120, 58, 428, '2019-11-04 07:42:24'),
(121, 58, 440, '2019-11-04 07:42:38'),
(126, 58, 439, '2019-11-04 08:32:45'),
(128, 203, 448, '2019-11-05 03:30:30'),
(129, 203, 449, '2019-11-05 03:30:32'),
(130, 203, 453, '2019-11-05 06:51:42'),
(134, 58, 461, '2019-11-05 10:30:31'),
(135, 58, 444, '2019-11-06 00:36:21'),
(136, 205, 480, '2019-11-07 02:03:53'),
(138, 205, 479, '2019-11-07 02:25:22'),
(140, 58, 458, '2019-11-07 05:44:44'),
(141, 206, 471, '2019-11-07 09:11:07'),
(143, 206, 480, '2019-11-08 13:54:23'),
(144, 206, 476, '2019-11-08 14:24:40'),
(145, 210, 485, '2019-11-09 05:25:45'),
(146, 210, 484, '2019-11-09 05:25:47'),
(149, 206, 482, '2019-11-09 08:17:14'),
(150, 206, 481, '2019-11-09 08:17:17'),
(153, 206, 462, '2019-11-09 08:40:17'),
(154, 206, 461, '2019-11-09 08:40:23'),
(156, 206, 463, '2019-11-09 13:36:10'),
(161, 207, 466, '2019-11-10 06:15:30'),
(162, 207, 469, '2019-11-10 06:31:10'),
(166, 207, 489, '2019-11-10 12:43:29'),
(168, 207, 472, '2019-11-10 12:45:24'),
(169, 207, 490, '2019-11-11 01:10:41'),
(170, 207, 487, '2019-11-11 01:26:25'),
(172, 212, 491, '2019-11-11 02:30:47'),
(174, 212, 493, '2019-11-11 03:36:12'),
(180, 206, 487, '2019-11-11 05:14:33'),
(181, 206, 483, '2019-11-11 05:14:36'),
(182, 206, 496, '2019-11-11 05:19:16'),
(183, 206, 491, '2019-11-11 05:20:21'),
(184, 206, 478, '2019-11-11 05:24:46'),
(185, 206, 490, '2019-11-11 05:35:55'),
(189, 205, 500, '2019-11-11 07:39:45'),
(192, 206, 497, '2019-11-11 12:32:36');

-- --------------------------------------------------------

--
-- テーブルの構造 `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `comment` longtext,
  `shared_post_id` bigint(20) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `comment`, `shared_post_id`, `image_name`, `created`, `updated`, `deleted`) VALUES
(458, 58, 'comment', NULL, NULL, '2019-11-07 05:43:18', NULL, NULL),
(459, 58, 'comment with image', NULL, '../Config/images/1573105471_d4eab97982348c5979e69b0adfab70303cdf2798.jpg', '2019-11-07 05:44:31', NULL, NULL),
(460, 58, 'comment for share', 459, NULL, '2019-11-07 05:50:09', NULL, NULL),
(461, 58, 'comment for share', 459, NULL, '2019-11-07 05:50:21', NULL, NULL),
(462, 58, '<script>location.reload();<script>', NULL, NULL, '2019-11-07 05:50:57', NULL, NULL),
(463, 207, 'edited comment for ', NULL, '../Config/images/1573107511_627815221444352fca16949e18269fd446386c96.jpg', '2019-11-07 06:09:31', '2019-11-10 00:26:21', 1),
(464, 209, 'comment for edit', NULL, '../Config/images/1573108704_7ec73590ddc83968db0d963197e87a855d4f2d90.jpg', '2019-11-07 06:24:57', '2019-11-07 06:40:00', 1),
(465, 209, 'comment for delete', 464, NULL, '2019-11-07 06:58:00', NULL, NULL),
(466, 209, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', NULL, NULL, '2019-11-07 07:01:09', NULL, NULL),
(467, 209, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', NULL, '../Config/images/1573110857_0c4f50da80e205178b722a6dd37e51dc5e90382e.jpg', '2019-11-07 07:01:26', '2019-11-07 07:14:17', NULL),
(468, 206, 'comment for delete', NULL, NULL, '2019-11-07 08:26:35', NULL, 1),
(469, 206, 'edited comment for share', NULL, NULL, '2019-11-07 08:27:45', '2019-11-08 14:27:19', NULL),
(470, 206, 'comment for share', 469, NULL, '2019-11-07 08:27:55', '2019-11-11 11:24:20', NULL),
(471, 206, '', 469, '../Config/images/1573115308_fab55a86b93abb813e929d2750b93ff008450a60.jpg', '2019-11-07 08:28:28', NULL, NULL),
(472, 206, 'share?', 469, NULL, '2019-11-07 08:28:54', NULL, NULL),
(473, 206, 'edited sample', NULL, NULL, '2019-11-07 09:06:37', '2019-11-07 09:08:21', 1),
(474, 206, 'aaaa', NULL, NULL, '2019-11-07 10:08:04', NULL, NULL),
(475, 206, 'deleted comment', NULL, NULL, '2019-11-08 07:57:53', NULL, 1),
(476, 206, 'comment with image', 469, '../Config/images/1573201406_5ed7e6a96e20383be6b94908ad18ddc7ff835422.jpg', '2019-11-08 08:23:26', '2019-11-08 08:23:40', NULL),
(477, 206, 'share with image', 469, NULL, '2019-11-08 08:25:37', NULL, 1),
(478, 206, 'comment with image', 469, '../Config/images/1573201799_c4894225a037d8af3049dfb9167c34b4d42ae8b1.jpg', '2019-11-08 08:29:59', NULL, NULL),
(479, 206, 'comment with image', NULL, '../Config/images/1573203096_7208f25c42bc1ffd1dcbb10ea672e0b55b43e690.jpg', '2019-11-08 08:51:36', NULL, 1),
(480, 206, 'edited comment', 479, NULL, '2019-11-08 08:51:57', '2019-11-08 13:58:57', 1),
(481, 206, 'image for share', 479, '../Config/images/1573222431_711e6bf73fbba76dda7cffaa3a545134d1bb72d0.jpg', '2019-11-08 14:11:53', '2019-11-08 14:26:41', NULL),
(482, 206, 'share ', 459, NULL, '2019-11-09 02:08:31', NULL, NULL),
(483, 206, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', NULL, NULL, '2019-11-09 02:12:28', NULL, NULL),
(484, 210, 'comment with image', NULL, '../Config/images/1573277104_41b011354d53a83548eba1cc53c68ca63d42fab5.jpg', '2019-11-09 05:25:04', NULL, 1),
(485, 210, 'share', 484, NULL, '2019-11-09 05:25:33', NULL, 1),
(486, 210, 'comment', NULL, NULL, '2019-11-09 05:28:20', NULL, 1),
(487, 206, 'sasadf', 462, NULL, '2019-11-09 13:36:54', '2019-11-11 06:24:31', NULL),
(488, 207, 'a', NULL, NULL, '2019-11-10 00:51:12', NULL, 1),
(489, 207, 'aaaaa', NULL, '../Config/images/1573366551_2a7536285316c8c073ced5571d59896d282369a6.jpg', '2019-11-10 06:15:51', NULL, NULL),
(490, 207, 'for share', 489, NULL, '2019-11-10 06:16:07', NULL, NULL),
(491, 207, 'aaa', 483, NULL, '2019-11-11 01:24:15', NULL, NULL),
(492, 212, 'comment', NULL, NULL, '2019-11-11 02:23:47', NULL, 1),
(493, 212, 'test', 483, NULL, '2019-11-11 02:25:01', NULL, NULL),
(494, 212, 'share with image', 483, NULL, '2019-11-11 03:36:41', NULL, 1),
(495, 212, 'share with image', 483, '../Config/images/1573443429_53a684432b64820d16308ffbec617ec897406af5.jpg', '2019-11-11 03:37:09', NULL, NULL),
(496, 206, 'edited comment', NULL, NULL, '2019-11-11 05:17:57', '2019-11-11 09:18:00', NULL),
(497, 206, 'from search', 489, NULL, '2019-11-11 05:36:43', NULL, NULL),
(498, 205, 'comment', NULL, NULL, '2019-11-11 07:30:25', NULL, 1),
(499, 205, 'comment', 498, NULL, '2019-11-11 07:30:31', NULL, NULL),
(500, 205, 'edited comment', 499, NULL, '2019-11-11 07:31:22', '2019-11-11 07:32:12', NULL),
(501, 206, '<script>location.reload();<script>', NULL, NULL, '2019-11-11 10:32:29', NULL, NULL),
(502, 206, '<script>location.reload();<script>', NULL, NULL, '2019-11-11 12:17:00', NULL, 1),
(503, 206, 'SELECT * FROM users', NULL, NULL, '2019-11-11 12:21:38', NULL, 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `profile_pictures`
--

CREATE TABLE `profile_pictures` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `images_name` varchar(255) DEFAULT 'users.png',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `profile_pictures`
--

INSERT INTO `profile_pictures` (`id`, `user_id`, `images_name`, `created`) VALUES
(119, 40, 'users.png', '2019-10-30 00:52:14'),
(120, 36, 'users.png', '2019-10-30 00:52:14'),
(123, 40, '1572415532_73eda165b08cdb3c509676b617a5fde5e49bcf1e.png', '2019-10-30 06:05:32'),
(124, 40, '1572415807_b778297878df5e9c1590a4457611ddfc09a601a4.png', '2019-10-30 06:10:07'),
(125, 40, '1572416943_ba3621375d3b2aec6d3a1f063192aff93a38d2cd.png', '2019-10-30 06:29:03'),
(126, 37, 'users.png', '2019-10-30 06:33:03'),
(127, 35, 'users.png', '2019-10-30 06:33:03'),
(128, 38, 'users.png', '2019-10-30 06:33:03'),
(129, 40, 'users.png', '2019-10-30 06:33:03'),
(130, 41, 'users.png', '2019-10-30 06:33:03'),
(131, 36, 'users.png', '2019-10-30 06:33:03'),
(133, 41, '1572417577_3b37a0c774a76f57cb822bb0ffec02a01d87dda7.png', '2019-10-30 06:39:37'),
(134, 37, 'users.png', '2019-10-30 06:40:06'),
(135, 35, 'users.png', '2019-10-30 06:40:06'),
(136, 38, 'users.png', '2019-10-30 06:40:06'),
(137, 40, 'users.png', '2019-10-30 06:40:06'),
(138, 41, 'users.png', '2019-10-30 06:40:06'),
(139, 42, 'users.png', '2019-10-30 06:40:06'),
(140, 36, 'users.png', '2019-10-30 06:40:06'),
(141, 45, 'users.png', '2019-10-30 07:11:38'),
(142, 47, 'users.png', '2019-10-30 07:32:56'),
(143, 47, '1572420816_c4b06fbceed8978ce7150245fd3538b0de8b01ea.png', '2019-10-30 07:33:36'),
(144, 48, 'users.png', '2019-10-30 07:34:50'),
(145, 47, 'users.png', '2019-10-30 07:32:56'),
(146, 47, '1572424571_6571cffa59ed70e04e21bb27a95aa55688ba61be.png', '2019-10-30 08:36:11'),
(147, 41, 'users.png', '2019-10-30 06:33:03'),
(148, 41, '1572424599_e8d3f8a209500a783ea748d6992fa62c1eb01fd6.png', '2019-10-30 08:36:39'),
(149, 40, 'users.png', '2019-10-30 00:52:14'),
(150, 40, '1572424696_6cf170a290062de99595f6b9b2553773521e7ebd.png', '2019-10-30 08:38:16'),
(151, 38, 'users.png', '2019-10-29 13:29:25'),
(152, 38, '1572440674_8dcb71e3c82bb4d961020f44fdbb4c063ee30fea.png', '2019-10-30 13:04:34'),
(153, 41, 'users.png', '2019-10-30 06:33:03'),
(154, 38, 'users.png', '2019-10-29 13:29:25'),
(155, 38, '1572487230_30f6b35fe39e215f12193acadab917d7d90326d5.png', '2019-10-31 02:00:30'),
(156, 38, 'users.png', '2019-10-29 13:29:25'),
(157, 48, 'users.png', '2019-10-30 07:34:50'),
(158, 48, '1572507057_8283c9822d9d5534db8bb5c2ea9a2708c197fba8.png', '2019-10-31 07:30:57'),
(159, 49, 'users.png', '2019-10-31 07:37:40'),
(160, 47, 'users.png', '2019-10-30 07:32:56'),
(161, 51, 'users.png', '2019-10-31 13:38:01'),
(162, 38, 'users.png', '2019-10-29 13:29:25'),
(163, 53, 'users.png', '2019-11-02 07:21:02'),
(164, 54, 'users.png', '2019-11-02 08:02:51'),
(165, 54, '1572688804_697b3b3c42599b64015980de85f2b4d9bd7fd9de.png', '2019-11-02 10:00:04'),
(166, 55, 'users.png', '2019-11-02 10:00:35'),
(167, 55, '1572690917_54e76eba168d6629fc80e91e0aa8b9d71e0e4bda.png', '2019-11-02 10:35:17'),
(168, 55, '1572691816_2661f3c37dc851bf6e1ffaa9805878a4ecda2430.png', '2019-11-02 10:50:16'),
(169, 53, 'users.png', '2019-11-02 07:21:02'),
(170, 38, 'users.png', '2019-10-29 13:29:25'),
(171, 38, '1572702473_9a21e78099707e1d3ff7e0996758c50833882c98.png', '2019-11-02 13:47:53'),
(172, 58, 'users.png', '2019-11-02 14:21:02'),
(173, 58, 'users.png', '2019-11-02 14:21:02'),
(174, 58, '1572704738_1cb81fae89b44b49651b8b03af1c64c7c5f2d7d3.jpg', '2019-11-02 14:25:38'),
(175, 58, '1572705015_92563f742cc5486f03981dc21fd69cbc9c0c2371.jpg', '2019-11-02 14:30:15'),
(176, 58, NULL, '2019-11-02 14:51:36'),
(177, 58, NULL, '2019-11-02 14:51:51'),
(178, 58, NULL, '2019-11-02 14:51:54'),
(179, 58, NULL, '2019-11-02 14:53:05'),
(180, 58, NULL, '2019-11-02 14:53:07'),
(181, 58, NULL, '2019-11-02 14:53:23'),
(182, 58, NULL, '2019-11-02 14:57:13'),
(183, 58, NULL, '2019-11-02 14:57:43'),
(184, 58, NULL, '2019-11-02 14:58:04'),
(185, 58, NULL, '2019-11-02 14:58:45'),
(186, 58, NULL, '2019-11-02 15:02:56'),
(187, 58, '1572707054_a81b48c035eeff2a0811bf39e03c7e45a8edb036.png', '2019-11-02 15:04:14'),
(188, 58, '1572707080_8289f99f0bc18b00f84f7b27364371d4a7890105.png', '2019-11-02 15:04:40'),
(189, 58, '1572707083_8e6a8f9cfa2a8e94b890ac5d64b8563a5c92492c.png', '2019-11-02 15:04:43'),
(190, 58, '1572707100_cab225fe1897972fcf7c0de94af1b2974d19d270.png', '2019-11-02 15:05:00'),
(191, 58, '1572707179_1afee0798530632c9f1b116193593ef8b43aa382.png', '2019-11-02 15:06:19'),
(192, 58, '1572707226_1e259af69e816fb718aaeb7c12ea51d26569f285.jpg', '2019-11-02 15:07:06'),
(193, 58, '1572707596_6dc8632377d84204fe51b881c01cde866e3512ce.jpg', '2019-11-02 15:13:16'),
(194, 58, '1572707675_e2dcd39c31237abba49f8f9e21ac734cabbbee26.png', '2019-11-02 15:14:35'),
(195, 47, 'users.png', '2019-10-30 07:32:56'),
(196, 58, 'users.png', '2019-11-02 14:21:02'),
(197, 58, '1572760904_52fff5857f64eb0a564a3530bdfb7657804aa52e.png', '2019-11-03 06:01:44'),
(198, 58, '1572762168_d69d018e5cd1a48d537bb78dafe83f2b2da61e74.png', '2019-11-03 06:22:48'),
(199, 58, '1572771730_d236e3e16cb8150099ee46cc54f0e6067c5ec7eb.png', '2019-11-03 09:02:10'),
(200, 60, 'users.png', '2019-11-03 10:21:41'),
(201, 60, '1572776996_f84ee223c632ceb04e75588c736b3a3309668f10.jpg', '2019-11-03 10:29:56'),
(202, 60, '1572777048_840867518c8fac49203bcba4897b2bddec5c5de5.png', '2019-11-03 10:30:48'),
(203, 60, '1572823946_8485d4c59211e65f90237d6e2ce66e200dcd2ca9.png', '2019-11-03 23:32:26'),
(204, 61, 'users.png', '2019-11-04 01:58:07'),
(205, 61, '1572832746_4bce970c4a669cf8b08e83181e8287692e7a5351.png', '2019-11-04 01:59:06'),
(206, 58, 'users.png', '2019-11-02 14:21:02'),
(207, 58, 'users.png', '2019-11-02 14:21:02'),
(208, 58, 'users.png', '2019-11-02 14:21:02'),
(209, 203, 'users.png', '2019-11-05 03:07:20'),
(210, 58, 'users.png', '2019-11-02 14:21:02'),
(211, 205, 'users.png', '2019-11-06 03:49:05'),
(212, 205, '1573090932_50fb5fef27338cc42e2f8cb87e2b1aa91b1d3283.png', '2019-11-07 01:42:12'),
(213, 58, 'users.png', '2019-11-02 14:21:02'),
(214, 58, '1573104164_01040c762b52f52d9abc35e516f60e468f0942b9.png', '2019-11-07 05:22:44'),
(215, 207, '1573106961_e84833b1ee4fb9decbecd3ff4e4b855ad2301fd2.png', '2019-11-07 06:09:21'),
(216, 208, 'users.png', '2019-11-07 06:22:43'),
(217, 209, 'users.png', '2019-11-07 06:24:20'),
(218, 206, '1573112981_b9e5632ba16607aba31a27db5ab858d8110b979c.png', '2019-11-07 07:49:41'),
(219, 206, '1573115455_c29a40c081bcf6217e7035cf4b35af674d7076f7.png', '2019-11-07 08:30:55'),
(220, 210, 'users.png', '2019-11-09 05:15:50'),
(221, 211, 'users.png', '2019-11-11 02:19:50'),
(222, 212, 'users.png', '2019-11-11 02:22:16'),
(223, 212, '1573439947_333e716a8b65cce04701e1da7ba6042fc12037a3.png', '2019-11-11 02:39:07'),
(224, 58, '1573459011_21ca2ba9f6abe719a06ad70cce56805e5d2b4eec.jpg', '2019-11-11 07:56:51'),
(225, 58, '1573459108_22a7cbf062521dde81367e1d8bd34eb815884722.jpg', '2019-11-11 07:58:28'),
(226, 58, '1573459134_384dd817ca8315b67db0e0f3ed5ff54844e4047b.jpg', '2019-11-11 07:58:54'),
(227, 58, '1573459172_a4c7e54d149536059bab845684fd91b4b0355023.png', '2019-11-11 07:59:32'),
(228, 58, '1573459301_83d4171158cad7373fe77a2fe782fec9522f8574.jpg', '2019-11-11 08:01:41'),
(229, 58, '1573459315_a2173bd83cd8b6fd254bf8ae65e72115c7f516f6.png', '2019-11-11 08:01:55'),
(230, 58, '1573459638_c4ae72c0983b74d4eeb2093aeb7b36a46399397a.jpg', '2019-11-11 08:07:18'),
(231, 58, '1573459646_ffe38980227832d540ba6d45ede1249c7f2c919b.png', '2019-11-11 08:07:26'),
(232, 206, '1573460164_a83baff14206f1891dbcfb97f770be258ee28231.png', '2019-11-11 08:16:04'),
(233, 206, '1573460199_843adf0fa74e2234dbc0683b58de50a8c2fb3a4f.jpg', '2019-11-11 08:16:39'),
(234, 206, '1573475860_bb3f466c0f18ed7af624d045154d8730be7a0860.jpg', '2019-11-11 12:37:40'),
(235, 206, '1573476037_2ab88ac559b056eca276d190b2029cbe72b82042.png', '2019-11-11 12:40:37');

-- --------------------------------------------------------

--
-- テーブルの構造 `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `created`) VALUES
(35, 'Test000', 'sample000@sample.com', '$2y$10$UWgCVyJGLsGTRr.5tcttAeC3T4tAoUGfqTLCuzF.a.QkfVrOltFfS', '2019-10-29 02:20:04'),
(36, 'Test001', 'sample0@sample.com', '$2y$10$AYUWwj0LUB65EKQ8VCgf5ufunxcQeyuayf4i92FnAjWcE2crDQxu.', '2019-10-29 02:21:31'),
(37, 'Test003', 'sample0005@sample.com', '$2y$10$dS9v6mwHMO.xD9buKzHZzek.N5./6dhJ2A63FVutEvsnw5O9Fr44u', '2019-10-29 07:36:59'),
(38, 'Test004', 'sample004@sample.com', '$2y$10$wroiI1gw9IfcSj37CSChCeAgumG7/JagrpkOjL.UcGEZtLJVtEGjm', '2019-10-29 13:29:25'),
(40, 'TEST006', 'sample006@sample.com', '$2y$10$O77m7vlgg/uVzp4r1zqfM.oiUkPUfcKMeEHFQzlt4u3ajE3VUtoYG', '2019-10-30 00:52:14'),
(41, 'Test007', 'sample007@sample.com', '$2y$10$LtaAcZFa4bQwXp92UIzc8eBr9SwCj9ZWqqyismys4Fd1H36UNhzOy', '2019-10-30 06:33:03'),
(42, 'Test008', 'sample008@sample.com', '$2y$10$85Vb8l/SSXMVphfwf33FselB1MljQ1jXXd1VEdYvDNljKDQl9td3e', '2019-10-30 06:40:06'),
(43, 'Test009', 'sample009@sample.com', '$2y$10$wNJ/5Co49wlPiBP3yC8ZSe9wRMc2UjzeIAD3RdcsmT59KgiS7N29e', '2019-10-30 06:41:53'),
(44, 'Test010', 'sample010@sample.com', '$2y$10$NWPFgP2vSUbXPZya2QKcueItu.6CjZ3/UOfdHZLvAs0lW00/2Nkx2', '2019-10-30 06:57:45'),
(45, 'Test011', 'sample011@sample.com', '$2y$10$Ay5viyhYYB/hzJYdr.F5Yeq06zQfMaOf3T7TfyokwZy1YD4Wp9tAO', '2019-10-30 07:11:38'),
(46, 'Test012', 'sample012@sample.com', '$2y$10$4uHmJ2/wnSapdGIzdicAHemu5y4sHX6fLyF6438pplV8oUnni8.Km', '2019-10-30 07:16:03'),
(47, 'TEST013', 'sample013@sample.com', '$2y$10$zFZDwGjejlA3joa3UTVL7ODL8.E5asC0046yaBEJdEQQAlF4jlPEG', '2019-10-30 07:32:56'),
(48, 'Test014', 'sample014@sample.com', '$2y$10$D22ifAzzbUwol2dB6phL4OqXqJ8XZWGyfNbWTEGUc0xNAViZY4BH2', '2019-10-30 07:34:50'),
(49, 'ABCabc123', 'ABC123@sample.com', '$2y$10$vGUihlFWwCt6RLtVOgNDeeP5CVMXmbgfViWHx27s6WAHPpM6hq2sW', '2019-10-31 07:37:40'),
(50, 'Test1', 'sample015@sample.com', '$2y$10$rjW01NHEhzusZh2amE9UR.X6oUmvK8gBPBZ/lkBQF54BCPnX.wdH.', '2019-10-31 09:11:31'),
(51, 'ryoji', 'nagai@gmail.com', '$2y$10$M3UpLOdnJPOHIcOjj8hBF.8ZLPBlZ7NOZBlqIb74wUjKviXhkh1Pq', '2019-10-31 13:38:01'),
(53, 'Test016', 'sample016@sample.com', '$2y$10$eACA.LCcwUIcTthsOX36N.4hq2M1zUrnRcgzqiu4zITu5kHO18wCC', '2019-11-02 07:21:02'),
(54, 'Test017', 'sample017@sample.com', '$2y$10$D1wS7ckUEV4tey.XiFipIeCokMOTin.RdRgUoyNiugcyWm//R14vm', '2019-11-02 08:02:51'),
(55, 'updatedTest019', 'sample019@sample.com', '$2y$10$y/enko5l.4ekTY18856c8.kt.TRRiDuCE99lWDcIKIjmIA.8aQy2.', '2019-11-02 10:00:35'),
(58, 'Test018', 'sample018@sample.com', '$2y$10$.fTGPX4CI7scstKl9wf.seNk3fMshXS8IIWzG/mabA.QEeDse7why', '2019-11-02 14:21:02'),
(60, 'updatedTest021', 'sample021@sample.com', '$2y$10$zsZPZgngG.BtYpUx3dDPMemi6tKxNGEng74zOXvx2tI7pdpnY.fx.', '2019-11-03 10:21:41'),
(61, 'Test020', 'sample020@sample.com', '$2y$10$3FWGoWMXBwdbIGJY46znVeZN8FVYzMrufvwRz1qrIX6Sik5HELD9a', '2019-11-04 01:58:07'),
(63, 'a', 'sample022@sample.com', '$2y$10$qFdr0m8lcLgusbBxhYcu9OfhpVWphh8Sady0r/.EXcOTF1VmNFIPm', '2019-11-04 07:46:33'),
(64, 'ryoji', 'sample023@sample.com', '$2y$10$BkpQ7IPUCFDTO0wB2FTa8uVzOIQwCwSDxQZLywE0tPnWV3pt0V...', '2019-11-04 07:48:59'),
(65, 'root', 'tora169mm@gmail.com', '$2y$10$ACbSJecmS/h9jFG8dqdmyO.RCcbLVEXdh4GGfl6UgMIdE2PfU.2NO', '2019-11-04 07:49:43'),
(67, 'root', '', '$2y$10$asRh6zJP/qXWjjWd1kweZumxubM.m8JUTzJbjdYwr11RwVa7drXm6', '2019-11-04 09:24:28'),
(90, 'root', 'sgdsfgsf', '$2y$10$cYPD/jhAISizyJJgePv78Oaj8T51h/Pn3MmXo.4IEdugTWbI2c6iO', '2019-11-04 09:54:26'),
(168, 'Test????', ' zzz', '$2y$10$10ZuwX8QGNaJ48kLzo8OTOoLpCxLSBxc1xCbm/tA18PcyTKuWYLES', '2019-11-04 11:49:48'),
(171, 'root', 'fafdsa', '$2y$10$JcvFsEQFvUsxYL5AEBllueiS1nEQ7nMcbyNsIhgcCbOhghg3fyZiy', '2019-11-04 11:50:58'),
(178, 'sample001', 'sample@sample.com', '$2y$10$T6EQqBqZ25WoAGkJ5WWIDO1DM5Xlg7dYhOGv2C7f8K32HWwsNOAiS', '2019-11-04 12:10:31'),
(201, 'Test023', 'sample024@sample.com', '$2y$10$nN477.wF8XbYVdxuITnwFOEj4nFECXnAwoFC6iSIgx8WDsnTWVb/e', '2019-11-05 02:30:25'),
(202, 'Test025', 'sample025@sample.com', '$2y$10$YVkYI/vsCXrG8q6DS5eJ.OWks2JIcv0pV0jJQVuoJZaLUwGCnLaw6', '2019-11-05 03:04:24'),
(203, 'Test026', 'sample026@sample.com', '$2y$10$V3XMV55CLuvLKYPM6Ir/bu92PPGX5x4ZR.hMg6xwZIRWX7GyeM/d.', '2019-11-05 03:07:20'),
(204, 'Test027', 'sample027@sample.com', '$2y$10$.g3e.l90afke6TxBTvHzaOydL6dWo13P4EZv.1rePhTg0QbStRo4q', '2019-11-06 03:47:32'),
(205, 'Test028', 'sample028@sample.com', '$2y$10$3YL7I9gkinpbSc5OmgVOquUGXvBgPo0TTdQSUbt5UHtnt6jeHKGsa', '2019-11-06 03:49:05'),
(206, 'updateTest029', 'sample029@sample.com', '$2y$10$7oM89NWaV4H7qMvpdCvce.Z0VHEHjpXWtdQ25rT8lIe6z.kpQrN4K', '2019-11-06 03:51:04'),
(207, 'Test030', 'sample030@sample.com', '$2y$10$YvnsGL/qkEHvvLrT37m0rOo6KocEdmfB7zfC0qi37mgJB73wSX9Mu', '2019-11-07 06:02:06'),
(208, 'Test031', 'sample031@sample.com', '$2y$10$rcHqPTZ7x3B6LU.x2kVfvOKCg4wrF57oII879uWVeTvpare8tSlGS', '2019-11-07 06:22:43'),
(209, 'UpdateTest032', 'updatesample032@sample.com', '$2y$10$mfFOgVt115xQUxqmcP3xcuxogQDLqWyvoz8bQqvNJeeI3NzlFaina', '2019-11-07 06:24:20'),
(210, 'Test033', 'sample033@sample.com', '$2y$10$6Oi0P24BxrDg.GPXA/1Ed.E7QhvafP44vUgAluuHftpWdt9doifN2', '2019-11-09 05:15:50'),
(211, 'kawasaki', 'kawasaki@gmail.com', '$2y$10$SXcYhV/6MRl7K1MdhBgyneQx6QG1e/OF5L79UPJmcO8ZoGjtwNAoO', '2019-11-11 02:19:50'),
(212, 'kawasaki1', 'kawasaki1@gmail.com', '$2y$10$.tTiEPpSCut8nTbq3bK47ueAZEeGeqeWdWgRsokn8oVUc51NiUX1S', '2019-11-11 02:22:16');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `profile_pictures`
--
ALTER TABLE `profile_pictures`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `followers`
--
ALTER TABLE `followers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=496;

--
-- テーブルのAUTO_INCREMENT `likes`
--
ALTER TABLE `likes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- テーブルのAUTO_INCREMENT `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=504;

--
-- テーブルのAUTO_INCREMENT `profile_pictures`
--
ALTER TABLE `profile_pictures`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;

--
-- テーブルのAUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
