<?php
require_once(__DIR__ . '/../Config/config.php');
// require_once(__DIR__ . '/../Lib/Controller/Login.php');
$app = new MyApp\Controller\Login();
$app->run();

list($success, $error) = $app->getResults();
 ?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8" />
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>Login</title>
 <link rel="stylesheet" href="sample_styles.css">
</head>
<body>
  <fieldset>
    <h1>Login</h1>
    <?php if (isset($success)) : ?>
    <div class="msg success"><?php echo h($success); ?></div>
    <?php endif; ?>
    <form method="post">
     <p><input type="text" name="email" placeholder="Email" value="<?= isset($app->getValues()->email) ? h($app->getValues()->email) : ''; ?>"/></p>
     <p><input type="password" name="password" placeholder="Password" /></p>
     <p class="err" id="err"><?= h($app->getErrors('login')); ?></p>
     <p><input type="submit" name="submit" value="LOGIN" /></p>
     <p><a href="signup.php">SIGN UP</a></p>
    </form>
    </fieldset>
    </body>
    <p> Created by Kenji</p>
</html>
