<?php
require_once(__DIR__.'/../Config/config.php');

$app = new MyApp\Controller\Update();
$app->run();
if (null === $_SESSION['update']){
  header('Location:login.php');
  exit;
}
 ?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8" />
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>Update</title>
 <link rel="stylesheet" href="sample_styles.css">
</head>
<body>
  <fieldset>
    <h1>Update</h1>
    <form  method="post">
      <?php if (null !== $app->getErrors('username')) : ?>
       <div class="msg error" id="err1"><?= h($app->getErrors('username')); ?></div>
      <?php endif; ?>
     <p><input type="text" name="username" placeholder="Username" value="<?= h($_SESSION['me']->username) ?>"/></p>
     <?php if (null !== $app->getErrors('email')) : ?>
      <div class="msg error" id="err1"><?= h($app->getErrors('email')); ?></div>
     <?php endif; ?>
     <p><input type="text" name="email" placeholder="Email"  value="<?= h($_SESSION['me']->email) ?>"/></p>
     <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
     <input type="submit" name="update" value="Update"/>
    </form>
  </fieldset>
</body>
    <p> Created by Kenji</a></p>
</html>
