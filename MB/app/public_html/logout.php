<?php

require_once(__DIR__ . '/../Config/config.php');

  $_SESSION = [];

  if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time() - 86400, '/');
  }

  session_destroy();



header('Location: index.php');
