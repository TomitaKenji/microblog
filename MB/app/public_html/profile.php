 <?php
 require_once(__DIR__.'/../Config/config.php');
 $id = $_GET['id'];

 $userModel = new \MyApp\Model\User();
 $postModel = new \MyApp\Model\Post();
 $userModel->userExist();
 $dbh = connectDb();
 $sql = "select count(*) from followers where user_id = :user_id and followed_id = :followed_id";
 $stmt = $dbh->prepare($sql);
 $stmt->execute([
   ':user_id' => $_SESSION['me']->id,
   ':followed_id' => $userModel->findUser($id)['user_id']
 ]);
 $r = $stmt->fetch(PDO::FETCH_ASSOC);
 $dbh = null;

 ?>
<!DOCTYPE html>
<html lang="en">
 <head>
  <title>Profile</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="home_styles.css">
   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
 </head>

<body>

<div class="header">
  <h1>Profile</h1>
</div>

 <div class="navbar">
   <form name="myForm" action="to_search.php" class="search-form"  method="post" onsubmit="return validateForm()">
     <input class="search-box" type="text" name="keyword" placeholder="Search">
     <input type="submit" name="search" value="search" class="button">
   </form>
  <div class="navi-items">
    <a class="navi-item" href="index.php">Home</a>
 </div>
  </div>
  <div class="row">
    <div class="side">
      <div class="side-item">
        <p><img class="profile-img" src="<?= $userModel->findUser($id)['images_name']?>"/></p>
        <p><?= $userModel->findUser($id)['username']; ?></p>
        <p><?= $userModel->findUser($id)['email']; ?></p>

        <?php if ($userModel->findUser($id)['user_id'] !== $_SESSION['me']->id) :?>
         <div class="follow-buttun">
          <?php if ( 0 == $r['count(*)'] ) : ?>
           <a href="#"  data-postid="<?= $userModel->findUser($id)['user_id'] ?>">Follow</a>
          <?php else: ?>
           <a href="#" class="follow-btn" data-postid="<?= $userModel->findUser($id)['user_id'] ?>">UnFollow</a>
          <?php endif; ?>
        </div>
      <?php endif; ?>
     </div>

      <ul>
     　　　 <p><div id="result" align="center"></div></p>
          <li>
           <div class="follow-btn">
              <a  href="following.php?id=<?= $id ?>">Following <?= count($userModel->followUser($id)) ?></a>
              <a  href="follower.php?id=<?= $userModel->findUser($id)['user_id'] ?>">Followers <?= count($userModel->followerUser($id)) ?></a>
           </div>
          </li>
      </ul>
    </div>

  <div class="main">
    <?php if( 0 === count($postModel->getUsercomment($id))) :?>
      <h2>NOT FOUND</h2>
    <?php endif; ?>

    <?php foreach($postModel->getUsercomment($id) as $comment) : ?>
   <?php if (null === $comment['deleted']) : ?>
    <ul class="post_list">
      <li class="post_item">
        <div class="post_info">
          <img class="profile_post_img" src="<?= $userModel->findUser($id)['images_name']?>"/>
          <?= $userModel->findUser($id)['username']?>

          <?php if (null === $comment['updated']) : ?>
           <div class="post-date"><?= date('M j(D) H:i', strtotime($comment['created']))?></div>
          <?php else : ?>
           <div class="post-date">Edited : <?= date('M j(D) H:i', strtotime($comment['updated']))?></div>
          <?php endif; ?>
       </div>

  　　　 <div class="post_content">
          <p class="post_text"><?= h($comment['comment'])?></p>

          <?php if (null !== $comment['image_name']) :?>
          <p><img class='post-img' src='<?= $comment['image_name']?>'/></p>
          <?php endif; ?>

          <?php if (null !== $comment["shared_post_id"]) : ?>
            <?php
            $dbh = connectDb();
            $sql = "select DISTINCT u.id, u.username, p.comment, p.image_name, p.created, p.updated, p.deleted, pp.images_name FROM users AS u LEFT OUTER JOIN posts AS p on u.id = p.user_id LEFT OUTER JOIN profile_pictures AS pp on u.id = pp.user_id where p.id = :shared_post_id order by pp.id DESC limit 1";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([
              ":shared_post_id" => $comment['shared_post_id']
            ]);
            $share_result = $stmt->fetch(PDO::FETCH_ASSOC);
            $dbh = null;
            ?>

            <?php if (null !== $share_result['deleted']): ?>
              <div class='share_comment'>
               <div class="share-delete">Deleted Comment amd Image</div>
             </div>
            <?php else:?>

             <div class='share_comment'>
               <div class='share_info'>
                <p class="share_name">
                  <img class='share_img' src="<?= $share_result['images_name']?>">
                   <a  href="profile.php?id=<?= h($share_result['id']); ?>"><?= h($share_result['username']) ?></a>
                </p>

               <?php if (null === $share_result['updated']) :?>
                 <div class='share-date'>
                  <?= $share_result['created']?>
                 </div>
               <?php else: ?>
                 <div class='share-date'>
                 Edited: <?= $share_result['updated']?>
                 </div>
               <?php endif;?>
              </div>
             <p class='share_text'><?= h($share_result['comment']) ?></p>
           <?php if (null !== $share_result['image_name']): ?>
               <p><img class='share-img' src='<?= $share_result['image_name']?>'></p>
           <?php endif ?>
            </div>
         <?php endif; ?>
        <?php endif; ?>


           <div class="post_btn">

             <?php if (null === $comment['shared_post_id']) :?>
              <a href="share.php?id=<?= $comment['id']; ?>" >Share</a>
            <?php elseif (null !== $comment['shared_post_id'] && null === $share_result['deleted'] ) :?>
              <a href="share.php?id=<?= $comment['shared_post_id']; ?>" >Share</a>
             <?php else :?>
              <a href="share.php?id=<?= $comment['id']; ?>" >Share</a>
             <?php endif ;?>
           </div>
          </li>
      </ul>
    <?php endif ; ?>
    <?php endforeach ; ?>

</div>
 <a href="#top" class="topBtn" id="topBtn">TOP</a>
 <div class="footer">
   <h2>Prototype</h2>
 </div>
</body>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>
    $(function() {
      $('#err').fadeOut(3000);
      $('.msg').fadeOut(3000);
      });

      $(document).ready(function(){
      $("#topBtn").hide();
      $(window).on("scroll", function() {
          if ($(this).scrollTop() > 100) {
              $("#topBtn").fadeIn("fast");
          } else {
              $("#topBtn").fadeOut("fast");
          }
          scrollHeight = $(document).height();
          scrollPosition = $(window).height() + $(window).scrollTop(); //現在地
          footHeight = $("footer").innerHeight();
          if ( scrollHeight - scrollPosition  <= footHeight ) {
              $("#topBtn").css({
                  "position":"absolute",
                  "bottom": footHeight + 30
              });
          } else {
              $("#topBtn").css({
                  "position":"fixed", //固定表示
                  "bottom": "20px" //下から20px上げた位置に
              });
          }
      });
      $('#topBtn').click(function () {
          $('body,html').animate({
          scrollTop: 0
          }, 400);
          return false;
      });
  });

  $(function(){
    $(".follow-buttun > a").click(function(event){
      var $this = $(this);
      var postid = ($(this).data("postid"));
      if($this.hasClass("follow-btn")){
        $.post('follow.php', {
          data: postid
        }).done(function (data) {
        $this.removeClass("follow-btn");
        $this.text("Follow");
        });
      } else {
        $.post('follow.php', {
          data: postid
        }).done(function (data) {
        $this.addClass("follow-btn");
        $this.text("UnFollow");
       });
      }
    });
  });

  function validateForm() {
    var value = document.forms["myForm"]["keyword"].value;
    var reg = new RegExp(/^\s+$/);
    if (value == "") {
      alert("Try searching username or keywords");
      return false;
    }
    var regex = new RegExp('[¥¥s]');
    if (reg.test(value)){
      alert("cant make empty!");
      return false;
    }
  }
</script>
</html>
