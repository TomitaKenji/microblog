<?php
 session_start();
if (isset($_POST['keyword'])){
  $_SESSION['keyword'] = $_POST['keyword'];
  $keywords = explode(" ", $_SESSION['keyword']);

  // キーワードの数だけループして、LIKE句の配列を作る
  $keywordCondition = [];
  foreach ($keywords as $keyword) {
      $commentkeywordCondition[] = 'p.comment LIKE "%' . $keyword . '%"';
      $usernamekeywordCondition[] = 'u.username LIKE "%' . $keyword . '%"';
  }
  // これをANDでつなげて、文字列にする
  $_SESSION['c_keyword'] = implode(' AND ', $commentkeywordCondition);
  $_SESSION['u_keyword'] = implode(' AND ', $usernamekeywordCondition);
  header('Location: search.php');
  exit;
}
?>
