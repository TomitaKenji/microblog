<?php
require_once(__DIR__.'/../Config/config.php');
require_once(__DIR__.'/../Lib/Controller/Post.php');
$id = $_GET['id'];

$app = new MyApp\Controller\Index();
$userModel = new \MyApp\Model\User();
$userModel->userExist();
$postModel = new \MyApp\Model\Post();
$app->run();

$dbh = connectDb();
$sql = "select count(*) from followers where user_id = :user_id and followed_id = :followed_id";
$stmt = $dbh->prepare($sql);
$stmt->execute([
  ':user_id' => $_SESSION['me']->id,
  ':followed_id' => $userModel->findUser($id)['user_id']
]);
$r = $stmt->fetch(PDO::FETCH_ASSOC);
$dbh = null;

?>
<!DOCTYPE html>
<html lang="en">
 <head>
  <title>Following</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="home_styles.css">
   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
   <script type="text/javascript" src="./footerFixed.js"></script>
 </head>

<body>
 <div class="header">
  <h1>Following</h1>
  <p>Microblog<b></b> created by Kenji.</p>
 </div>

 <div class="navbar">
   <form name="myForm" action="to_search.php" class="search-form"  method="post" onsubmit="return validateForm()">
     <input class="search-box" type="text" name="keyword" placeholder="Search">
     <input type="submit" name="search" value="search" class="button">
   </form>
  <div class="navi-items">
    <a class="navi-item" href="index.php">Home</a>
  </div>
</div>
<div class="row">
  <div class="side">
    <div class="side-item">
      <p><img class="profile-img" src="<?= $userModel->findUser($id)['images_name']?>"/></p>
      <p><?= $userModel->findUser($id)['username']; ?></p>
      <p><?= $userModel->findUser($id)['email']; ?></p>
      <div class="follow-buttun">
        <?php if ( $_SESSION['me']->id !== $id) : ?>
          <?php if ( 0 == $r['count(*)'] ) : ?>
           <a href="#" data-postid="<?= $id ?>">Follow</a>
          <?php else: ?>
           <a href="#"  class="follow-btn" data-postid="<?= $id ?>">UnFollow</a>
          <?php endif; ?>
        <?php endif; ?>
      </div>

   </div>

  <div class="follow-btn">
    <a>Following <?= count($userModel->followUser($id)) ?></a>
    <a  href="follower.php?id=<?= $userModel->findUser($id)['user_id'] ?>">Followers <?= count($userModel->followerUser($id)) ?></a>
  </div>
</div>

<div class="main">
  <?php if ( 0 === count($userModel->followUser($id))) :?>
    <h2>NOT FOUND</h2>
  <?php endif; ?>

  <?php foreach($userModel->followUser($id) as $follow):?>
    <?php

      $dbh = connectDb();
      $sql = "select images_name from profile_pictures where user_id = :user_id order by id desc limit 1";
      $stmt = $dbh->prepare($sql);
      $stmt->execute([
        ':user_id' => $follow['followed_id']
      ]);
      $res = $stmt->fetch(PDO::FETCH_ASSOC);
      $sql = "select count(*) from followers where user_id = :user_id and followed_id = :followed_id";
      $stmt = $dbh->prepare($sql);
      $stmt->execute([
        ':user_id' => $_SESSION['me']->id,
        ':followed_id' => $follow['followed_id']
      ]);
      $r = $stmt->fetch(PDO::FETCH_ASSOC);
      $dbh = null;
      ?>
      <div class="following-info">
        <div class="follow-item">
          <img class="follow-img" src="<?= $res['images_name']?>"/>
          <div class="follow-contents">
            <div class="follow-content">
              <a  href="profile.php?id=<?= $follow['followed_id'] ?>"><?= $follow['username']?></a>
            </div>
            <div class="follow-content">
              E-mail :  <?= $follow['email']?>
            </div>
            <div class="follow-content">
              <div class="follow-buttun">
                <?php if ( $_SESSION['me']->id !== $follow['followed_id']) : ?>
                  <?php if ( 0 == $r['count(*)'] ) : ?>
                   <a href="#"  data-postid="<?= $follow['followed_id'] ?>">Follow</a>
                  <?php else: ?>
                   <a href="#"  class="follow-btn" data-postid="<?= $follow['followed_id'] ?>">UnFollow</a>
                  <?php endif; ?>
                <?php endif; ?>
              </div>

              <a  href="following.php?id=<?= $follow['followed_id'] ?>">Following <?= count($userModel->followUser($follow['followed_id'])) ?></a>
              <a  href="follower.php?id=<?= $follow['followed_id'] ?>">Followers <?= count($userModel->followerUser($follow['followed_id'])) ?></a>
            </div>
          </div>
        </div>
      </div>
   <?php endforeach; ?>
  </div>
</div>
<div class="footer">
  <h2>Prototype</h2>
</body>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>

$(function(){
  $(".follow-buttun > a").click(function(event){
    var $this = $(this);
    var postid = ($(this).data("postid"));
    if($this.hasClass("follow-btn")){
      $.post('follow.php', {
        data: postid
      }).done(function (data) {
      $this.removeClass("follow-btn");
      $this.text("Follow");
      });
    } else {
      $.post('follow.php', {
        data: postid
      }).done(function (data) {
      $this.addClass("follow-btn");
      $this.text("UnFollow");
     });
    }
  });
});

function validateForm() {
  var value = document.forms["myForm"]["keyword"].value;
  var reg = new RegExp(/^\s+$/);
  if (value == "") {
    alert("Try searching username or keywords");
    return false;
  }
  var regex = new RegExp('[¥¥s]');
  if (reg.test(value)){
    alert("cant make empty!");
    return false;
  }
}
</script>
</html>
