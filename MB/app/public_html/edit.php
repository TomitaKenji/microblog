<?php
require_once(__DIR__.'/../Config/config.php');
require_once(__DIR__.'/../Lib/functions.php');
$postModel = new \MyApp\Model\Post();
$postModel->cheackPost();
$app = new \MyApp\Controller\Post();
$app->edit();
$app->run();

 ?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8" />
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>Edit/Delete</title>
 <link rel="stylesheet" href="sample_styles.css">
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
</head>
<body>
  <fieldset>
    <h1>EDIT/DELETE</h1>
    <form action="" method="post" enctype="multipart/form-data">
     <p class="err" id="err"><?= h($app->getErrors('comment'))?></p>
     <p><textarea class="edit-textarea" type="text" name="comment"><?= ht($app->getValues()->edit['comment']) ?></textarea></p>
     <?php if (null !== $app->getValues()->edit['image_name']) : ?>
      <img class='post-img' src="<?=$app->getValues()->edit['image_name'] ?>">
     <?php endif; ?>
     <input type="hidden" name="MAX_FILE_SIZE" value="<?= h(MAX_FILE_SIZE); ?>">
     <label class="file-btn-label" for="file-sample">
       SELECT IMAGE
     <input class="file-btn" type="file" name="image" id="file-sample"/>
     </label>
     <span></span>
     <p><input type="submit" name="edit" value="SAVE"/></p>
     <input type="hidden"  name="posts_id" value="<?= h($_POST['posts_id'])?>">
     <p><a href="javascript:history.back();">Back</a></p>
    </form>
    </fieldset>
    </body>
    <p> Created by Kenji</p>
    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script>
        $(function() {
            $('#err').fadeOut(2000);
          });

        $(function() {
          $('#file-sample').on('change', function(e) {
          var file = $(this).prop('files')[0];
          $('span').html(file.name);
        });
      });
    </script>
</html>
