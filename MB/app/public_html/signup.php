<?php
require_once(__DIR__.'/../Config/config.php');
require_once(__DIR__.'/../Lib/functions.php');

$app = new MyApp\Controller\Signup();
$app->run();

 ?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8" />
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>Singup</title>
 <link rel="stylesheet" href="sample_styles.css">
</head>
<body>
  <fieldset>
    <h1>Signup </h1>

    <form  method="post">
      <div class="tooltip">Username
       <span class="tooltiptext">Please input using half-width English letters, more than 5 but less than 15</span>
      </div>
      <p><input type="text" name="username" placeholder="Username" value="<?= isset($app->getValues()->username) ? h($app->getValues()->username) : ''; ?>"/></p>
       </div>
       <?php if (null != $app->getErrors('username')) : ?>
         <div class="balloon">
           <div class="msg error" id="err1"><?= h($app->getErrors('username')); ?></div>
        </div>
          <?php endif; ?>
     <div class="tooltip">E-mail
      <span class="tooltiptext">It should be a corrected format</span>
     </div>
     <p><input type="text" name="email" placeholder="email" value="<?= isset($app->getValues()->email) ? h($app->getValues()->email) : ''; ?>"/></p>
     <?php if (null != $app->getErrors('email')) : ?>
       <div class="balloon">
        <div class="msg error" id="err1"><?= h($app->getErrors('email')); ?></div>
      </div>
     <?php endif; ?>
     <div class="tooltip">Password
      <span class="tooltiptext">Please input using half-width English numbers and letters, more than 8 characters</span>
     </div>
     <p><input type="password" name="password" placeholder="Password" /></p>
     <?php if (null != $app->getErrors('password')) : ?>
     <div class="balloon">
       <div class="msg error" id="err1"><?= h($app->getErrors('password')); ?></div>
     </div>
     <?php endif; ?>
     <p><input type="password" name="confirmed_password" placeholder="Confirmed Password" /></p>
     <?php if (null != $app->getErrors('confirmed_password')) : ?>
     <div class="balloon">
       <div class="msg error" id="err1"><?= h($app->getErrors('confirmed_password')); ?></div>
     </div>
     <?php endif; ?>
     <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
     <input type="submit" name="submit" value="Enter"/>
     <p><a href="login.php">LOGIN</a></p>
    </form>
  </fieldset>
</body>
    <p>Created by Kenji</p>
</div>
</html>
