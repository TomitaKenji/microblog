<?php

require_once(__DIR__ . '/../Config/config.php');
require_once(__DIR__.'/../Lib/functions.php');

  if (isset($_POST['data'])){
    $dbh = connectDb();
    $sql = "select count(*) from followers where user_id= :user_id and followed_id= :followed_id;";
    $stmt = $dbh->prepare($sql);
    $stmt->execute([
      ":user_id" => $_SESSION['me']->id,
      ":followed_id" => $_POST['data']
    ]);
    $count = $stmt->fetch();
    $msg = array();
    if ($count['count(*)'] == 0){
        $sql = "insert into followers (user_id, followed_id, created)
                values (:user_id, :followed_id, now())";
        $stmt = $dbh->prepare($sql);
        $stmt->execute([
          ":user_id" => $_SESSION['me']->id,
          ":followed_id" => $_POST['data']
        ]);

   } else {
        $sql = "delete from followers where user_id= :user_id and followed_id = :followed_id limit 1";
        $stmt = $dbh->prepare($sql);
        $stmt->execute([
          ":user_id" => $_SESSION['me']->id,
          ":followed_id" => $_POST['data']
        ]);

     }
   }
