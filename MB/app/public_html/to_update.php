<?php
require_once(__DIR__.'/../Config/config.php');

$app = new MyApp\Controller\Update();
$app->run();
 ?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8" />
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>Update</title>
 <link rel="stylesheet" href="sample_styles.css">
</head>
<body>
  <fieldset>
    <h1>Change information</h1>
    <form  method="post">
     <p><input type="password" name="password" placeholder="Password" /></p>
     <p><input type="password" name="confirmed_password" placeholder="Confirmed Password" /></p>
     <?php if (null !== $app->getErrors('password')) : ?>
      <div class="msg error" id="err1"><?= h($app->getErrors('password')); ?></div>
     <?php endif; ?>
     <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
     <input type="submit" name="toUpdate" value="Enter"/>
     <p><a href="index.php">back</a></p>
    </form>
  </fieldset>
</body>
    <p> Created by Kenji</a></p>
</html>
