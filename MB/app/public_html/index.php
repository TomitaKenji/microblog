<?php
require_once(__DIR__.'/../Config/config.php');
require_once(__DIR__.'/../Lib/Controller/Post.php');
require_once(__DIR__.'/../Lib/Controller/ImageUploader.php');

$_SESSION['page'] = "index.php";
//paging
$page = isset($_GET['page']) ? h($_GET['page']) : 1;
$offset = COMMENTS_PER_PAGE * ($page - 1);
$dbh = connectDb();
$sql = "select DISTINCT p.id, p.comment, p.shared_post_id, u.username, f.followed_id FROM posts AS p INNER JOIN users AS u ON p.user_id = u.id INNER JOIN profile_pictures AS pp ON p.user_id = pp.user_id INNER JOIN followers AS f ON p.user_id =f.followed_id where f.user_id = :id and p.deleted is null and p.commented_post_id is null ORDER BY `p`.`id` DESC";
$stmt = $dbh->prepare($sql);
$stmt->execute([
  ':id' => $_SESSION['me']->id
]);
$comments = $stmt->fetchAll(PDO::FETCH_ASSOC);
$total = count($comments);
$totalPages = ceil($total / COMMENTS_PER_PAGE);

$app = new MyApp\Controller\Index();
$app->run();
$post = new \MyApp\Controller\Post();
$post->post();
list($success, $error) = $post->getResults();
$userModel = new \MyApp\Model\User();
$dbh = connectDb();

$post->delete();
?>
<!DOCTYPE html>
<html lang="ja">
<div class="wrapper">
 <head>
  <title>Home</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="home_styles.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
 </head>

<body>
 <header class="header" id="top">
  <h1>Prototype</h1>

 </div>

 <div class="navbar">
  <div class="navi-items">
    <a class="navi-item" href="logout.php">Logout</a>
    <a class="navi-item" href="picture.php">Picture</a>
    <a class="navi-item" href="to_update.php">Update Information</a>
  </div>

  <form name="myForm" action="to_search.php" class="search-form"  method="post" onsubmit="return validateForm()">
    <input class="search-box" type="text" name="keyword" placeholder="Search">
    <input type="submit" name="search" value="search" class="button">
  </form>
 </div>

 <div class="row">
   <div class="side">
     <div class="side-item">
       <h2>About Me</h2>
       <p><img class="profile-img" src="<?=$app->getValues()->me['images_name']?>"/></p>
       <p>Welcome! <?= h($app->getValues()->me['username'])?></p>
    </div>

    <div class="follow-btn">
      <a  href="following.php?id=<?= $_SESSION['me']->id ?>">Following <?= count($app->getValues()->follows) ?></a>
      <a  href="follower.php?id=<?= $_SESSION['me']->id ?>">Followers <?= count($app->getValues()->followers) ?></a>
    </div>
  </div>

  <div class="main">
    <h2>POST</h2>

       <form class="post-form" method="post" enctype="multipart/form-data">
        <textarea type="text" name="comment"  placeholder="What's happening?"></textarea><br>
        <p class="err" id="err"><?= h($post->getErrors('comment'))?></p>
        <div class="post-form-btn">
         <input type="hidden" name="MAX_FILE_SIZE" value="<?= h(MAX_FILE_SIZE); ?>">
         <label class="post-input-label" for="file-sample">
           SELECT IMAGE
         <input class="post-input-btn" type="file" name="image" id="file-sample"/>
         </label>
         <input type="submit" name="submit" value="POST" class="button">
        </div>
      </form>
      <span></span>
      <?php if (isset($success)) : ?>
      <div class="msg success"><?= h($success); ?></div>
      <?php endif; ?>

      <?php if (isset($error)) : ?>
       <div class="msg error"><?= h($error); ?></div>
      <?php endif; ?>

      <hr />

    <?php foreach ($app->getValues()->comments as $comment) : ?>

          <ul class="post_list">
           <li class="post_item">
            <div class="post_info">
             <img class="profile_post_img" src="<?= $comment['images_name']?>"/>
             <a class="post-username" href="profile.php?id=<?= h($comment['followed_id']); ?>"><?= h($comment['username']); ?></a>
             <?php if (null === $comment['updated']) : ?>
             <div class="post-date" id=" "><?= date('M j(D) H:i', strtotime($comment['created']))?></div>
             <?php else : ?>
             <div class="post-date"><?= date('M j(D) H:i', strtotime($comment['updated']))?></div>
             <?php endif; ?>

             <?php if ($_SESSION['me']->id === $comment['followed_id']): ?>
             <form id="" method="POST">
               <input type="hidden" name="delete" value="<?= $comment['id']?>" >
               <button class="delete-btn" type="submit" id="btn">X</button>
             </form>
            <?php endif; ?>

            </div>
　　　　　  <div class="post_content">
            <p class="post_text"><?= h($comment['comment'])?></p>
             <?php if (null !== $comment['image_name']): ?>　
              <p><img class='post-img' src='<?= $comment['image_name']?>'/></p>
             <?php endif; ?>
          </div>


          <?php if (null !== $comment['shared_post_id']) :?>
            <?php
            $dbh = connectDb();
            $sql = "select DISTINCT u.id, u.username, p.comment, p.image_name, p.created, p.updated, p.deleted, pp.images_name FROM users AS u
            LEFT OUTER JOIN posts AS p on u.id = p.user_id
            LEFT OUTER JOIN profile_pictures AS pp on u.id = pp.user_id
            where p.id = :shared_post_id order by pp.id DESC limit 1";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([
              ":shared_post_id" => $comment['shared_post_id']
            ]);
            $share_result = $stmt->fetch(PDO::FETCH_ASSOC);
            $dbh = null;
            ?>

          <?php if (null !== $share_result['deleted']): ?>
            <div class='share_comment'>
              <p class="share_name">
                <img class='share_img' src="<?= $share_result['images_name']?>">
                 <a  href="profile.php?id=<?= h($share_result['id']); ?>"><?= h($share_result['username']) ?>
                </a>
              </p>
             <div class="share-delete">Deleted Comment and Image</div>

           </div>
          <?php else:?>

           <div class='share_comment'>
             <div class='share_info'>
              <p class="share_name">
                <img class='share_img' src="<?= $share_result['images_name']?>">
                 <a  href="profile.php?id=<?= h($share_result['id']); ?>"><?= h($share_result['username']) ?>
                </a>
              </p>

             <?php if (null === $share_result['updated']) :?>
               <div class="share-date"><?= date('M j(D) H:i', strtotime($comment['created']))?></div>
             <?php else: ?>
               <div class="share-date">Edited: <?= date('M j(D) H:i', strtotime($comment['updated']))?></div>
             <?php endif;?>
            </div>
           <p class='share_text'><?= h($share_result['comment']) ?></p>
         <?php if (null !== $share_result['image_name']): ?>
             <p><img class='share-img' src='<?= $share_result['image_name']?>'></p>
         <?php endif ?>
          </div>
       <?php endif; ?>
      <?php endif; ?>

         <div class="post_btn">
           <?php if ($_SESSION['me']->id === $comment['followed_id']): ?>
            <a href="edit.php?id=<?= h($comment['id'])?>">Edit</a>
           <?php endif; ?>

           <?php
           $dbh = connectDb();
           $sql = "select count(posts_id) from likes where posts_id = :posts_id";
           $stmt = $dbh->prepare($sql);
           $stmt->execute([
             ":posts_id" => $comment['id']
           ]);
           $likes_result = $stmt->fetch(PDO::FETCH_ASSOC);
           $sql = "select * from likes where posts_id = :posts_id and user_id = :user_id";
           $stmt = $dbh->prepare($sql);
           $stmt->execute([
             ":posts_id" => $comment['id'],
             ":user_id" => $_SESSION['me']->id
           ]);
           $like_comment = $stmt->fetch(PDO::FETCH_ASSOC);
           // var_dump($like_comment);
           ?>
          <div class="like-buttun">
           <?php if ( false === $like_comment) : ?>
             <a href="#" data-likesid="<?=$comment['id']?>">Like</a>
            <?php else: ?>
             <a href="#" class="like-btn" data-likesid="<?=$comment['id']?>">UnLike</a>
            <?php endif; ?>
             <label><?= h($likes_result['count(posts_id)']); ?></label>
          </div>

           <?php if (null === $comment['shared_post_id']) :?>
            <a href="share.php?id=<?= $comment['id']; ?>" >Share</a>
          <?php elseif (null !== $comment['shared_post_id'] && null === $share_result['deleted'] ) :?>
            <a href="share.php?id=<?= $comment['shared_post_id']; ?>">Share</a>
           <?php else :?>
            <a href="share.php?id=<?= $comment['id']; ?>">Share</a>
           <?php endif ;?>
            <a href="comment.php?id=<?= $comment['id']; ?>">Comment</a>
          </div>
         </li>
        </ul>
      <?php endforeach; ?>

      <hr />
    <?php if ($page > 1) : ?>
      <a  class="pager" href="?page=<?=  $page-1; ?>">Prev</a>
      <?php endif; ?>

    <?php for ($i = 1; $i <= $totalPages; $i++) : ?>
      <!-- total page = 1 cant see pagination-->
      <?php if ( 1 == $totalPages) :?>
      <?php else: ?>

      <?php if ($page == $i) :?>
        <a class="pagers" href="?page=<?=  $i ?>" ><?=  $i; ?></a>
      <?php else : ?>
        <a class="pager" href="?page=<?=  $i ?>" ><?=  $i; ?></a>
      <?php endif; ?>

      <?php endif; ?>
      <?php endfor; ?>
      <?php if ($page < $totalPages) : ?>
     <a class="pager" href="?page=<?= $page+1; ?>">Next</a><br />
     <?php endif; ?>
     <a href="#top" class="topBtn" id="topBtn">TOP</a>
 </div>

<div class="footer">
  <h2>Prototype</h2>
</div>
</div>
</body>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>
$('button').click(function(){
    if(!confirm('Do you really want to delete this post?')){
        /* キャンセルの時の処理 */
        return false;
    }else{
        /*　OKの時の処理 */
        return true
    }
});
    $(function() {
      $('.err').fadeOut(3000);
      $('.msg').fadeOut(3000);
      });

      $(document).ready(function(){
      $("#topBtn").hide();
      $(window).on("scroll", function() {
          if ($(this).scrollTop() > 100) {
              $("#topBtn").fadeIn("fast");
          } else {
              $("#topBtn").fadeOut("fast");
          }
          scrollHeight = $(document).height();
          scrollPosition = $(window).height() + $(window).scrollTop(); //現在地
          footHeight = $(".footer").innerHeight();
          if ( scrollHeight - scrollPosition  <= footHeight ) {
              $("#topBtn").css({
                  "position":"absolute",
                  "bottom": footHeight + 30
              });
          } else {
              $("#topBtn").css({
                  "position":"fixed", //固定表示
                  "bottom": "20px" //下から20px上げた位置に
              });
          }
      });
      $('#topBtn').click(function () {
          $('body,html').animate({
          scrollTop: 0
          }, 400);
          return false;
      });
  });

  $(function() {
    $('#file-sample').on('change', function(e) {
    var file = $(this).prop('files')[0];
    $('span').html(file.name);
  });
});


  $(function(){
    $(".like-buttun > a").click(function(event){
      var $this = $(this);
      var likesid = ($(this).data("likesid"));
      if($this.hasClass("like-btn")){
        $.post('likes.php', {
          data: likesid
        }).done(function (data) {
        $this.removeClass("like-btn");
        $this.text("Like");
        $this.next().html(data);
        });
      } else {
        $.post('likes.php', {
          data: likesid
        }).done(function (data) {
        $this.addClass("like-btn");
        $this.text("UnLike");
        $this.next().html(data);
       });
      }
    });
  });

  function validateForm() {
    var value = document.forms["myForm"]["keyword"].value;
    var reg = new RegExp(/^\s+$/);
    if (value == "") {
      alert("Try searching username or keywords");
      return false;
    }
    var regex = new RegExp('[¥¥s]');
    if (reg.test(value)){
      alert("cant make empty! Try searching username or keywords");
      return false;
    }
  }
  target = document.getElementById("date");
            target.innerHTML = "Penguin"
  var datetime = '2019/11/18 09:25:42';
    var from = new Date(datetime);

    // 現在時刻との差分＝経過時間
    var diff = new Date().getTime() - from.getTime();
    // 経過時間をDateに変換
    var elapsed = new Date(diff);

    // 大きい単位から順に表示
    if (elapsed.getUTCFullYear() - 1970) {
      console.log(elapsed.getUTCFullYear() - 1970 + '年前');
    } else if (elapsed.getUTCMonth()) {
      console.log(elapsed.getUTCMonth() + 'ヶ月前');
    } else if (elapsed.getUTCDate() - 1) {
      console.log(elapsed.getUTCDate() - 1 + '日前');
    } else if (elapsed.getUTCHours()) {
      console.log(elapsed.getUTCHours() + 'h');
    } else if (elapsed.getUTCMinutes()) {
      console.log(elapsed.getUTCMinutes() + 'm');
    } else {
      elapsed.getUTCSeconds() + 'right now';
    }
</script>
</html>
