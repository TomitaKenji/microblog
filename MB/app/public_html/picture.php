<?php
require_once(__DIR__.'/../Config/config.php');
require_once(__DIR__.'/../Lib/functions.php');

$app = new MyApp\Controller\Index();
$app->run();

$uploader = new MyApp\Controller\ImageUploader();
if (isset($_POST['upload_image'])) {
 $uploader->upload();
}
list($success, $error) = $uploader->getResults();
 ?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8" />
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>Profile</title>
 <link rel="stylesheet" href="sample_styles.css">
</head>

<body>
  <fieldset>
    <h1>Profile</h1>
    <form action="" method="post" enctype="multipart/form-data">
      <p><?=h($app->getValues()->me['username']) ?></p>
      <p><img class="profile-img" src="<?= h($app->getValues()->me['images_name'])?>"/></p>
      <p>Email : <?= h($app->getValues()->me['email'])?></p>
      <label class="file-btn-label" for="file-sample">
        SELECT IMAGE
      <input class="file-btn" type="file" name="image" id="file-sample"/>
      </label>
      <span></span>
      <input type="hidden" name="MAX_FILE_SIZE" value="<?= h(MAX_FILE_SIZE); ?>">
      <p><input type="submit" name="upload_image" value="upload"></p>
      <?php if (isset($error)) : ?>
       <div class="msg error"><?= h($error); ?></div>
      <?php endif; ?>
      </form>
      <p><a href="index.php" >back</a></p>
   </form>
  </fieldset>
</body>

    <p> Created by Kenji</p>

    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script>
        $(function(){
            $('.msg').fadeOut(3000);
            });

        $(function() {
          $('#file-sample').on('change', function(e) {
          var file = $(this).prop('files')[0];
          $('span').html(file.name);
        });
      });
    </script>
</html>
