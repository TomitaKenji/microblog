<?php
require_once(__DIR__.'/../Config/config.php');
require_once(__DIR__.'/../Lib/Controller/Post.php');
$_SESSION['page'] = 'search.php';
  $page = isset($_GET['page']) ? h($_GET['page']) : 1;
  $offset = COMMENTS_PER_PAGE * ($page - 1);
  $userModel = new \MyApp\Model\User();
  $app = new MyApp\Controller\Index();
  $app->run();

  // SELECT文にくっつけru
  $dbh = connectDb();
  $sql = "SELECT DISTINCT p.id, p.comment, p.image_name, p.shared_post_id, p.created, p.updated, u.username, p.user_id
  FROM posts AS p INNER JOIN users AS u ON p.user_id = u.id INNER JOIN profile_pictures AS pp ON p.user_id = pp.user_id
  WHERE " . $_SESSION['c_keyword'] . " And p.deleted IS NULL";
  $stmt = $dbh->prepare($sql);
	$stmt->execute();
  $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
  $total = count($res);
  $totalPages = ceil($total / COMMENTS_PER_PAGE);
  $sql = "SELECT DISTINCT p.id, p.comment, p.image_name, p.shared_post_id, p.created, p.updated, u.username, p.user_id
  FROM posts AS p INNER JOIN users AS u ON p.user_id = u.id INNER JOIN profile_pictures AS pp ON p.user_id = pp.user_id
  WHERE " . $_SESSION['c_keyword'] . " And p.deleted IS NULL ORDER BY p.id DESC limit ".$offset.",".COMMENTS_PER_PAGE;
  $stmt = $dbh->prepare($sql);
	$stmt->execute();
  $comments = $stmt->fetchAll(PDO::FETCH_ASSOC);

  $sql = "SELECT DISTINCT u.id, u.username, u.email FROM users AS u INNER JOIN profile_pictures AS pp ON u.id = pp.user_id where " . $_SESSION['u_keyword'] . " ORDER BY u.username";
  $stmt = $dbh->prepare($sql);
	$stmt->execute();
  $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
  $dbh = null;

  $from = $offset + 1;
  $to = ($offset + COMMENTS_PER_PAGE) < $total ? ($offset + COMMENTS_PER_PAGE) : $total;

?>
<!DOCTYPE html>
<html lang="en">
 <head>
  <title>Search</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="home_styles.css">
   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
 </head>

<body>

<div class="header">
  <h1>Search</h1>
</div>

 <div class="navbar">
   <form name="myForm" action="to_search.php" class="search-form"  method="post" onsubmit="return validateForm()">
     <input class="search-box" type="text" name="keyword" placeholder="Search">
     <input type="submit" name="search" value="search" class="button">
   </form>
  <div class="navi-items">
    <a class="navi-item" href="index.php">Home</a>
  </div>
 </div>
 <?php if (count($users) == 0 && count($comments) == 0):?>
   <div class="not-contents">
     <div class="not-content">
       <h2>Search Word :<?= h($_SESSION['keyword']) ?></h2>
       <h2>Suggestions:</h2>
     </div>
     <div class="not-content">
       <p class="not-msg">
         Make sure that all words are spelled correctly.
       </p>
       <p class="search-msg">
         Try different keywords.
       </p>
       <p class="search-msg">
         Try more general keywords.
       </p>
       <p class="search-msg">
         Try fewer keywords.
       </p>
       <p class="search-msg">
        don't exist page...
       </p>
     </div>
   </div>
 <?php else: ?>
  <div class="row">
    <div class="side">
      <ul>
        <h2>Search Word :<?= $_SESSION['keyword'] ?></h2>
        <h2>User Result : (<?= count($users)?>)</h2>
        <?php foreach ($users as $user) : ?>
          <?php
            $dbh = connectDb();
            $sql = "select images_name from profile_pictures where user_id = :user_id order by id desc limit 1";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([
              ":user_id" => $user['id']
            ]);
            $res = $stmt->fetch(PDO::FETCH_ASSOC);
            $sql = "select count(*) from followers where user_id = :user_id and followed_id = :followed_id";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([
              ':user_id' => $_SESSION['me']->id,
              ':followed_id' => $user['id']
            ]);
            $r = $stmt->fetch(PDO::FETCH_ASSOC);
            $dbh = null;
            ?>

          <li>
           <div class="search-inf">
              <div class="search-item">
               <img class="search-img" src="<?= $res['images_name']?>"/>
               <div class="search-contents">
                 <div class="search-content">
                   <a href="profile.php?id=<?= $user['id']; ?>"><?= $user['username']; ?></a>
                 </div>
                 <div class="search-content">
                   E-mail : <?= $user['email']; ?>
                 </div>
                 <div class="search-content">
                  <div class="follow-buttun">
                    <?php if ( $_SESSION['me']->id !== $user['id']) : ?>
                      <?php if ( 0 == $r['count(*)'] ) : ?>
                       <a href="#" data-postid="<?= $user['id'] ?>">Follow</a>
                      <?php else: ?>
                       <a href="#" class="follow-btn" data-postid="<?= $user['id'] ?>">UnFollow</a>
                      <?php endif; ?>
                    <?php endif; ?>
                  </div>

                   <a href="following.php?id=<?= $user['id'] ?>">Following <?= count($userModel->followUser($user['id'])) ?></a>
                   <a href="follower.php?id=<?= $userModel->findUser($user['id'])['user_id'] ?>">Followers <?= count($userModel->followerUser($user['id'])) ?></a>
                 </div>
                </div>
               </div>
             </div>
           </li>
         <?php endforeach; ?>
       </ul>
    </div>

  <div class="main">
    <ul class="post_list">
      <h2>Search Word  :  <?= $_SESSION['keyword'];?></h2>
      <h2>Post Result  : (<?= $total?>)</h2>
      <p>The current page viewed <?= $from ?>〜<?= $to ?></p>
      <?php foreach ($comments as $comment) : ?>
      <?php

        $dbh = connectDb();
        $sql = "select images_name from profile_pictures where user_id = :user_id order by id desc limit 1";
        $stmt = $dbh->prepare($sql);
        $stmt->execute([
          ":user_id" => $comment['user_id']
        ]);
        $res = $stmt->fetch(PDO::FETCH_ASSOC);
        $dbh = null;

        ?>

            <ul class="post_list">
             <li class="post_item">
              <div class="post_info">
               <img class="profile_post_img" src="<?= $res['images_name']?>"/>
               <a class="post-username" href="profile.php?id=<?= $comment['user_id']; ?>"><?= h($comment['username']); ?></a>
               <?php if (null === $comment['updated']) : ?>
               <div class="post-date"><?= date('M j(D) H:i', strtotime($comment['created']))?></div>
               <?php else : ?>
               <div class="post-date">Edited : <?= date('M j(D) H:i', strtotime($comment['updated']))?></div>
               <?php endif; ?>
              </div>

  　　　　　  <div class="post_content">
              <p class="post_text"><?= h($comment['comment'])?></p>
               <?php if (null !== $comment['image_name']): ?>　
                <p><img class='post-img' src='<?= $comment['image_name']?>'/></p>
               <?php endif; ?>
            </div>


            <?php if (null !== $comment['shared_post_id']) :?>
              <?php
              $dbh = connectDb();
              $sql = "select DISTINCT u.id, u.username, p.comment, p.image_name, p.created, p.updated, p.deleted, pp.images_name
              FROM users AS u LEFT OUTER JOIN posts AS p on u.id = p.user_id
              LEFT OUTER JOIN profile_pictures AS pp on u.id = pp.user_id
              where p.id = :shared_post_id order by pp.id DESC limit 1";
              $stmt = $dbh->prepare($sql);
              $stmt->execute([
                ":shared_post_id" => $comment['shared_post_id']
              ]);
              $share_result = $stmt->fetch(PDO::FETCH_ASSOC);
              $dbh = null;
              ?>

            <?php if (null !== $share_result['deleted']): ?>
              <div class='share_comment'>
               <div class="share-delete">Deleted Comment and Image</div>
             </div>
            <?php else:?>

             <div class='share_comment'>
               <div class='share_info'>
                <p class="share_name">
                  <img class='share_img' src="<?= $share_result['images_name']?>">
                   <a  href="profile.php?id=<?= h($share_result['id']); ?>"><?= h($share_result['username']) ?>
                  </a>
                </p>

               <?php if (null === $share_result['updated']) :?>
                 <div class='share-date'>
                  <?= date('M j(D) H:i', strtotime($share_result['created']))?>
                 </div>
               <?php else: ?>
                 <div class='share-date'>
                 Edited: <?= date('M j(D) H:i', strtotime($share_result['updated']))?>
                 </div>
               <?php endif;?>
              </div>
             <p class='share_text'><?= h($share_result['comment']) ?></p>
           <?php if (null !== $share_result['image_name']): ?>
               <p><img class='share-img' src='<?= $share_result['image_name']?>'></p>
           <?php endif ?>
            </div>
         <?php endif; ?>
        <?php endif; ?>

           <div class="post_btn">
             <?php if ($_SESSION['me']->id === $comment['user_id']): ?>
              <a href=edit.php?id=<?= $comment['id']?>>Edit/Delete</a>
             <?php endif; ?>
             <?php if (null === $comment['shared_post_id']) :?>
              <a href="share.php?id=<?= $comment['id']; ?>" >Share</a>
            <?php elseif (null !== $comment['shared_post_id'] && null === $share_result['deleted'] ) :?>
              <a href="share.php?id=<?= $comment['shared_post_id']; ?>" >Share</a>
             <?php else :?>
              <a href="share.php?id=<?= $comment['id']; ?>" >Share</a>
             <?php endif ;?>
          </div>
        </li>
      </ul>
    <?php endforeach; ?>

    <hr />
    <?php if ($page > 1) : ?>
      <a  class="pager" href="?page=<?=  $page-1; ?>">Prev</a>
    <?php endif; ?>

    <?php for ($i = 1; $i <= $totalPages; $i++) : ?>
    <!-- total page = 1 cant see pagination-->
    <?php if ( 1 != $totalPages) :?>
      <?php if ($page == $i) :?>
        <a class="pagers" href="?page=<?=  $i ?>" ><?=  $i; ?></a>
      <?php else : ?>
        <a class="pager" href="?page=<?=  $i ?>" ><?=  $i; ?></a>
      <?php endif; ?>
      <?php endif; ?>
    <?php endfor; ?>
    <?php if ($page < $totalPages) : ?>
      <a class="pager" href="?page=<?= $page+1; ?>">Next</a><br />
    <?php endif; ?>
    </div>
    </div>

 <a href="#top" class="topBtn" id="topBtn">TOP</a>

 <div class="footer">
   <h2>Prototype</h2>
 </div>
<?php endif ;?>
</body>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>
$(function(){
  $(".follow-buttun > a").click(function(event){
    var $this = $(this);
    var postid = ($(this).data("postid"));
    if($this.hasClass("follow-btn")){
      $.post('follow.php', {
        data: postid
      }).done(function (data) {
      $this.removeClass("follow-btn");
      $this.text("Follow");
      });
    } else {
      $.post('follow.php', {
        data: postid
      }).done(function (data) {
      $this.addClass("follow-btn");
      $this.text("UnFollow");
     });
    }
  });
});

function validateForm() {
  var value = document.forms["myForm"]["keyword"].value;
  var reg = new RegExp(/^\s+$/);
  if (value == "") {
    alert("Try searching username or keywords");
    return false;
  }
  var regex = new RegExp('[¥¥s]');
  if (reg.test(value)){
    alert("cant make empty!");
    return false;
  }
}
</script>
</html>
