<?php
require_once(__DIR__.'/../Config/config.php');
require_once(__DIR__.'/../Lib/Controller/Post.php');
require_once(__DIR__.'/../Lib/Controller/ImageUploader.php');
$_SESSION['page'] = 'comment.php?id='.$_GET['id'];

$app = new MyApp\Controller\Index();
$app->run();
$post = new \MyApp\Controller\Post();
$post->comment();
$post->delete();

list($success, $error) = $post->getResults();

$dbh = connectDb();
$postModel = new \MyApp\Model\Post();
$comment = $postModel->getPost();
$postModel->cheackComment();
$postModel->existPost();

?>
 <!DOCTYPE html>
<html lang="ja">
<div class="wrapper">
 <head>
  <title>Home</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="home_styles.css">

 </head>

<body>
 <header class="header" id="top">
  <h1>Prototype</h1>

 </div>

 <div class="navbar">
  <div class="navi-items">
    <a class="navi-item" href="logout.php">Logout</a>
    <a class="navi-item" href="index.php">Home</a>
  </div>
    <form action="search.php" class="search-form"  method="post">
      <input class="search-box" type="text" name="keyward" placeholder="Search">
      <input type="submit" name="search" value="search" class="button">
    </form>
 </div>

 <div class="row">
   <div class="side">
     <div class="side-item">
       <h2>About Me</h2>
       <p><img class="profile-img" src="<?=$app->getValues()->me['images_name']?>"/></p>
       <p>Welcome! <?= h($app->getValues()->me['username'])?></p>
    </div>

    <div class="follow-btn">
      <a  href="following.php?id=<?= $_SESSION['me']->id ?>">Following <?= count($app->getValues()->follows) ?></a>
      <a  href="follower.php?id=<?= $_SESSION['me']->id ?>">Followers <?= count($app->getValues()->followers) ?></a>
    </div>
  </div>

  <div class="main">
    <h2>Comment</h2>
      <hr />
          <ul class="post_list">
           <li class="post_item">
            <div class="post_info">
             <img class="profile_post_img" src="<?= h($comment['images_name'])?>"/>
             <a class="post-username" href="profile.php?id=<?= h($comment['user_id']); ?>"><?= h($comment['username']); ?></a>


             <?php if (null === $comment['updated']) : ?>
               <div class="post-date"><?= date('M j(D) H:i', strtotime($comment['created']))?></div>
             <?php else : ?>
               <div class="post-date">Edited : <?= date('M j(D) H:i', strtotime($comment['updated']))?></div>
             <?php endif; ?>

             <?php if ($_SESSION['me']->id === $comment['user_id']): ?>
               <form  method="POST">
                 <input type="hidden" name="delete" value="<?= $comment['id']?>" >
                 <button class="delete-btn" type="submit" id="btn">X</button>
               </form>
             <?php endif; ?>

            </div>

　　　　　  <div class="post_content">
            <p class="post_text"><?= h($comment['comment'])?></p>
             <?php if (null !== $comment['image_name']): ?>　
              <p><img class='post-img' src='<?= $comment['image_name']?>'/></p>
             <?php endif; ?>
          </div>


          <?php if (null !== $comment['shared_post_id']) :?>
            <?php
            $dbh = connectDb();
            $sql = "select DISTINCT u.id, u.username, p.comment, p.image_name, p.created, p.updated, p.deleted, pp.images_name FROM users AS u
            LEFT OUTER JOIN posts AS p on u.id = p.user_id
            LEFT OUTER JOIN profile_pictures AS pp on u.id = pp.user_id
            where p.id = :shared_post_id order by pp.id DESC limit 1";
            $stmt = $dbh->prepare($sql);
            $stmt->execute([
              ":shared_post_id" => $comment['shared_post_id']
            ]);
            $share_result = $stmt->fetch(PDO::FETCH_ASSOC);
            $dbh = null;
            ?>

          <?php if (null !== $share_result['deleted']): ?>
             <p class="share_name">
               <img class='share_img' src="<?= $share_result['images_name']?>">
                <a  href="profile.php?id=<?= h($share_result['id']); ?>"><?= h($share_result['username']) ?>
               </a>
             </p>
             <div class='share_comment'>
              <div class="share-delete">Deleted Comment and Image</div>
           </div>
          <?php else:?>

           <div class='share_comment'>
             <div class='share_info'>
              <p class="share_name">
                <img class='share_img' src="<?= $share_result['images_name']?>">
                 <a  href="profile.php?id=<?= h($share_result['id']); ?>"><?= h($share_result['username']) ?>
                </a>
              </p>

             <?php if (null === $share_result['updated']) :?>
               <div class="share-date"><?= date('M j(D) H:i', strtotime($comment['created']))?></div>
             <?php else: ?>
               <div class="post-date">Edited :<?= date('M j(D) H:i', strtotime($comment['updated']))?></div>
             <?php endif;?>
            </div>
           <p class='share_text'><?= h($share_result['comment']) ?></p>
         <?php if (null !== $share_result['image_name']): ?>
             <p><img class='share-img' src='<?= h($share_result['image_name']) ?>'></p>
         <?php endif; ?>
          </div>
       <?php endif; ?>
      <?php endif; ?>

         <div class="post_btn">
           <?php if ($_SESSION['me']->id === $comment['user_id']): ?>
            <a href="edit.php?id=<?= h($comment['id'])?>">Edit</a>
           <?php endif; ?>

           <?php

           $dbh = connectDb();
           $sql = "select count(posts_id) from likes where posts_id = :posts_id";
           $stmt = $dbh->prepare($sql);
           $stmt->execute([
             ":posts_id" => $comment['id']
           ]);
           $likes_result = $stmt->fetch(PDO::FETCH_ASSOC);
           $sql = "select * from likes where posts_id = :posts_id and user_id = :user_id";
           $stmt = $dbh->prepare($sql);
           $stmt->execute([
             ":posts_id" => $comment['id'],
             ":user_id" => $_SESSION['me']->id
           ]);
           $like_comment = $stmt->fetch(PDO::FETCH_ASSOC);
           ?>
           <div class="like-buttun">
            <?php if ( false === $like_comment) : ?>
              <a href="#" data-likesid="<?=$comment['id']?>">Like</a>
             <?php else: ?>
              <a href="#" class="like-btn" data-likesid="<?=$comment['id']?>">UnLike</a>
             <?php endif; ?>
              <label><?= h($likes_result['count(posts_id)']); ?></label>
           </div>

           <?php if (null === $comment['shared_post_id']) :?>
            <a href="share.php?id=<?= $comment['id']; ?>" >Share</a>
          <?php elseif (null !== $comment['shared_post_id'] && null === $share_result['deleted'] ) :?>
            <a href="share.php?id=<?= $comment['shared_post_id']; ?>" >Share</a>
           <?php else :?>
            <a href="share.php?id=<?= $comment['id']; ?>" >Share</a>
           <?php endif ;?>
         </div>
        </li>
           <?php

           $dbh = connectDb();
           $sql = "select DISTINCT p.id, p.user_id, u.username, p.comment, p.image_name, p.created, p.updated, p.deleted, pp.images_name FROM users AS u
           LEFT OUTER JOIN posts AS p on u.id = p.user_id
           LEFT OUTER JOIN profile_pictures AS pp on u.id = pp.user_id
           where p.commented_post_id = :id order by pp.id DESC ";
           $stmt = $dbh->prepare($sql);
           $stmt->execute([
             ":id" => $_GET['id']
           ]);
           $c_r = $stmt->fetchAll(PDO::FETCH_ASSOC);
           $dbh = null;

           ?>
           <?php if( 0 !== count($c_r)) :?>
           <?php foreach ($c_r as $toComment): ?>
              <hr />
             <div class="post_info">
               <img class="profile_post_img" src="<?= $toComment['images_name']?>"/>
                 <a class="post-username" href="profile.php?id=<?= h($toComment['user_id']); ?>">
                   <?= h($comment['username']); ?></a>
                 <?php if (null == $toComment['updated']) : ?>
                   <div class="post-date"><?= date('M j(D) H:i', strtotime($toComment['created']))?>
                   </div>
                 <?php else : ?>
                  <div class="post-date">Edited :<?= date('M j(D) H:i', strtotime($toComment['updated']))?>
                  </div>
                 <?php endif; ?>

                 <?php if (null === $toComment['deleted']): ?>
                   <form  method="POST">
                     <input type="hidden" name="delete" value="<?= $toComment['id']?>" >
                     <button type="submit" class="delete-btn" id="delete">X</button>
                   </form>
                 <?php endif ; ?>
             </div>

  　　　　　  <div class="post_content">
              <?php if (null !== $toComment['deleted']): ?>
                <div share_comment>
                  <div class="share-delete">Deleted Comment and Image</div>
                </div>
              <?php else : ?>
             <p class="post_text"><?= h($toComment['comment'])?></p>
                <?php if (null !== $toComment['image_name']): ?>　
                 <p><img class='post-img' src='<?= $toComment['image_name']?>'/></p>
                <?php endif; ?>

              <div class="post_btn">
                <?php if ($_SESSION['me']->id === $toComment['user_id']): ?>
                 <a href=edit.php?id=<?= h($toComment['id'])?>>Edit</a>
                <?php endif; ?>
           　　</div>
             <?php endif; ?>
            </div>

         <?php endforeach; ?>
       <?php endif; ?>
     </ul>

       <form action="" class="post-form" method="post" enctype="multipart/form-data">
        <textarea type="text" name="comment"  placeholder="What's happening?"></textarea><br>
        <div class="post-form-btn">
          <input type="hidden" name="MAX_FILE_SIZE" value="<?= h(MAX_FILE_SIZE); ?>">
            <label class="post-input-label" for="file">SELECT IMAGE
              <input class="post-input-btn" type="file" name="image" id="file"/>
            </label>
        <input type="submit" name="submit" value="COMMENT" class="button">
      </div>
    </form>
    <span></span>
      <p class="err" id="err"><?= h($post->getErrors('comment'))?></p>
      <?php if (isset($success)) : ?>
      <div class="msg success"><?= h($success); ?></div>
      <?php endif; ?>

      <?php if (isset($error)) : ?>
       <div class="msg error"><?= h($error); ?></div>
      <?php endif; ?>

      <hr />

     <a href="#top" class="topBtn" id="topBtn">TOP</a>
 </div>

<div class="footer">
  <h2>Prototype</h2>
</div>
</div>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>
 'use strict';
$(function() {
  $('#err').fadeOut(3000);
  $('.msg').fadeOut(3000);
});

$(function() {
  $('#file').on('change', function(e) {
  var file = $(this).prop('files')[0];
  $('span').html(file.name);
  });
});

$(function(){
  $(".like-buttun > a").click(function(event){
    var $this = $(this);
    var likesid = ($(this).data("likesid"));
    if($this.hasClass("like-btn")){
      $.post('likes.php', {
        data: likesid
      }).done(function (data) {
      $this.removeClass("like-btn");
      $this.text("Like");
      $this.next().html(data);
      });
    } else {
      $.post('likes.php', {
        data: likesid
      }).done(function (data) {
      $this.addClass("like-btn");
      $this.text("UnLike");
      $this.next().html(data);
     });
    }
  });
});
setTimeout(function() {
      window.scroll(0,$(document).height());
  },0);

 function validateForm() {
  var value = document.forms["myForm"]["keyword"].value;
  var reg = new RegExp(/^\s+$/);
  if (value == "") {
    alert("Try searching username or keywords");
    return false;
  }
  var regex = new RegExp('[¥¥s]');
  if (reg.test(value)){
    alert("cant make empty!");
    return false;
  }
}

$('#btn').click(function(){
    if(!confirm('Do you really want to delete this post?')){
        /* キャンセルの時の処理 */
        return false;
    }else{
        /*　OKの時の処理 */
        return true
    }
});
</script>
</body>
</html>
