<?php
 require_once(__DIR__.'/../Lib/functions.php');
 require_once(__DIR__.'/../Config/config.php');

 $app = new \MyApp\Controller\Post();
 $app->share();
 $postModel = new \MyApp\Model\Post();
 $postModel->existPost();

 list($success, $error) = $app->getResults();
 $app->run();
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8" />
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>Share</title>
 <link rel="stylesheet" href="sample_styles.css">
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
 </head>
<body>
  <fieldset>
    <h1>Share</h1>
    <div class="share_comment">
     <p class="share_info"><img class="share_img" src="<?=$app->getValues()->share['images_name']?>">
       <?= h($app->getValues()->share['username']) ?></p>
     <p class="share_text"><?= h($app->getValues()->share['comment'])?></p>
     <?php if (null !== $app->getValues()->share['image_name']):?>
     <p><img class='post-img' src='<?= $app->getValues()->share['image_name']?>'/></p>
     <?php endif; ?>
    </div>

    <form class="share-form" action="" method="post" enctype="multipart/form-data">
     <p class="err" id="err"><?= h($app->getErrors('comment'))?></p>
     <p><textarea type="text" name="comment"  placeholder="What's happening?"></textarea></p>
     <p><input type="hidden" name="MAX_FILE_SIZE" value="<?= h(MAX_FILE_SIZE); ?>"></p>
     <label class="file-btn-label" for="file-sample">
       SELECT IMAGE
     <input class="file-btn" type="file" name="image" id="file-sample"/>
     </label>
       <span></span>


     <?php if (isset($success)) : ?>
     <div class="msg success"><?= $success ?></div>
     <?php endif; ?>
     <?php if (isset($error)) : ?>
      <div class="msg error"><?= $error ?></div>
     <?php endif; ?>

     <p><input type="submit" name="share" value="Share" /></p>
     <p><a href"javascript:void(0);" onclick="history.go(-1)">Back</a></p>
    </form>
    </fieldset>
    </body>
    <p> Created by Kenji</p>
    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script>
        $(function(){
            $('#err').fadeOut(2000);
            $('.msg').fadeOut(3000);
            });

        $(function() {
          $('#file-sample').on('change', function(e) {
          var file = $(this).prop('files')[0];
          $('span').html(file.name);
        });
        });
    </script>
</html>
