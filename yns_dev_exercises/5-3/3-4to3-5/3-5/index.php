<?php

require_once('config.php');
require_once('functions.php');

if (empty($_SESSION['me'])) {
    header('Location:login.php');
    exit;
}
if (isset($_POST['logout'])){

  unset($_SESSION['me']);
  session_destroy();
  header("Location: login.php");
  exit;
}
$name = ($_SESSION['me']['first_name']);
$image = ($_SESSION['me']['picture_id']);

$_GET['page'] ='';
if (preg_match('/^[1-9][0-9]*$/', $_GET['page'])) {
    $page = (int)$_GET['page'];
} else {
    $page = 1;
}
$dbh = connectDb();
$offset = COMMENTS_PER_PAGE * ($page - 1);
$sql = "select first_name, last_name, email, password from users limit ".$offset.", ".COMMENTS_PER_PAGE;
$users = array();
foreach ($dbh->query($sql) as $row) {
    array_push($users, $row);
}
$total = $dbh->query("select count(*) from users")->fetchColumn();
$totalPages = ceil($total / COMMENTS_PER_PAGE);
//var_dump($users);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>Home</title>
<link rel="stylesheet" href="styles.css">
</head>
<body>
  <div class="container">
  <div class="main" align="center">
   <h2>Welcome! as <?php echo h($name);?></h2>
   <p><img src="images/<?php echo $image ?>"></p>
   </p>
  </div>
  <div class="side">
    <table border=1 align="center">
      <h2>UserList</h2>
    <?php foreach($users as $user){ ?>
    <tr>
        <?php foreach($user as $cel){ ?>
            <td><?= $cel?></td>
        <?php } ?>
    </tr>
    <?php } ?>
</table>
<div class="page" align="center">
  <?php if ($page > 1) : ?>
    <a href="?page=<?php echo $page-1; ?>">Prev</a>
    <?php endif; ?>
<?php for ($i = 1; $i <= $totalPages; $i++) : ?>
    <a href="?page=<?php echo $i; ?>" ><?php echo $i; ?></a>
    <?php endfor; ?>
    <?php if ($page < $totalPages) : ?>
   <a href="?page=<?php echo $page+1; ?>">Next</a><br />
   <?php endif; ?>
   <form action="" method="post">
     <input type="submit" name="logout" class="btn-flat-border" value="logout">
   </form>
  </div>
  </div>
  </div>
</body>
</html>
